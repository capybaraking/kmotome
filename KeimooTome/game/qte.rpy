screen qte_keyboard:
    #key input qte

    timer interval repeat True action If(time_start > 0.0, true=SetVariable('time_start', time_start - interval), false=[Return(0), Hide('qte_keyboard')])
    # timer, using variables from label qte_setup
    # false is the condition if the timer runs out - and this will be reached if the user doesn't get hit the key on time

    key trigger_key action Return(1)
    key "r" action Return (0)
    key "q" action Return (0)
    key "s" action Return (0)
    key "d" action Return (0)
    key "f" action Return (0)
    key "t" action Return (0)
    key "y" action Return (0)
    key "u" action Return (0)
    key "i" action Return (0)
    key "o" action Return (0)
    key "p" action Return (0)
    key "g" action Return (0)
    key "h" action Return (0)
    key "j" action Return (0)
    key "k" action Return (0)
    key "l" action Return (0)
    key "m" action Return (0)
    key "w" action Return (0)
    key "x" action Return (0)
    key "c" action Return (0)
    key "v" action Return (0)
    key "b" action Return (0)
    key "n" action Return (0)
    key "&" action Return (0)
    key "é" action Return (0)
    key "3" action Return (0)
    key "1" action Return (0)
    key "2" action Return (0)
    
    if(trigger_key != "a") :
        key "a" action Return (0)
    elif(trigger_key != "z") :
        key "z" action Return (0)
    elif(trigger_key != "e") :
        key "e" action Return(0)
    
    frame :
        background "#000"
        xalign x_align
        yalign y_align
        vbox:
            
            spacing 25
            # vbox arrangement

            text trigger_key:
                xalign 0.5
                color "#fff"
                size 36
                #outlines [ (2,"#000000",0,0) ]
                # text showing the key to press

            bar:
                value time_start
                range time_max
                xalign 0.5
                xmaximum 300
                if time_start < (time_max * 0.25):
                    left_bar "#f00"
                    # this is the part that changes the colour to red if the time reaches less than 25%