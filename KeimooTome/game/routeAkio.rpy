label routeAkiobegin :
    
    #GLOBAL VARIABLES
    $ numberAkio = False #donner son numéro à Akio
    $ vengeance1 = False
    $ goodEndAkio = 0 #points pour la good end Akio
    
    #Deuxieme jour de cours de MC
    
    scene room
    "Bip…Bip…Bip."
    "Mon réveil m’extrait d’un sommeil sans rêve. J’ouvre lentement les yeux et fais face aux mêmes murs froids que la veille."
    "Je traîne longuement sous l’eau brûlante de la douche. Personne ne pourra me reprocher de le faire de toute façon."
    
    scene kitchen

    "Je retourne ensuite m’habiller dans ma chambre, puis je prends la direction de la cuisine. Mon ventre grogne, mais je ne suis toujours pas motivé pour cuisiner quelque chose."
    "Tant pis, j’achèterai une barre chocolatée sur le chemin."
    "J’attrape mon sac et quitte la maison, direction le lycée."
    
    scene classroom
    
    "Quelques élèves sont déjà présents dans la salle et discutent avec entrain. Pour ma part, je me fais discret et reprends la même place que la veille. "
    "Je profite du fait que le prof ne soit pas encore arrivé pour manger ce que j’ai acheté au distributeur."
    "Crunch. Crunch. Les bruits de la mastication couvriraient presque le brouhaha ambiant. "
    
    show hayden normal
    
    Hayden "Bonjour à tous !"
    
    "Le prof fait l'appel, puis la séance débute."
    
    hide hayden normal
    
    menu:
        "C'est parti pour 3h30 de mathématiques."
        "Prendre des notes":
            "J’ai toujours été bon élève en mathématiques."
            "Je n’ai pas du mal à suivre ce que dit Yoshida-sensei et prends des notes."
            "Je suis si concentré que je ne vois pas le temps passer."
            
        "Regarder dehors":
            "Les mathématiques ne m’ont jamais passionnées."
            "Je décroche au bout de quelques minutes à peine et laisse mon regard vagabonder vers l’extérieur où d’autres élèves font des tours de terrain pour le cours de sport."
            "Le prof ne semble pas vraiment remarquer que j’aie la tête ailleurs et le cours devient un peu plus supportable dans ces conditions."
            
        "Dormir":
            "Je ne résiste pas longtemps à l’envie de bâiller, puis celle de poser ma tête sur mes bras et enfin celle de fermer les yeux. "
            "La voix mélodieuse de Yoshida-sensei expliquant la résolution des équations du second degré se fait de plus en plus lointaine, et je finis par m’endormir."
            "....ZZzzZ…."
            "Lorsque je reprends connaissance, plusieurs élèves ont le regard tourné vers moi avec l’air amusé, mais pas le prof."
            "Je me redresse et fais semblant de prendre des notes durant le reste de l’heure."
            
    "La fin du cours finit par arriver. Yoshida-sensei décide de ne pas nous donner de devoirs pour nous laisser le temps de nous adapter à notre nouvelle classe."
    "Alors que tout le monde se rue vers l’extérieur de la salle de classe pour être le premier à la cafétéria, je reste assis et gribouille dans mon cahier."
    "La porte s’ouvre, je n’y prête pas attention jusqu’à ce que la personne soit devant moi."
    
    show akio normal
    
    Akio "Salut !"

    "Il a l’air en meilleure forme qu’hier. Ca me rassure."

    Moi "Salut."
    Akio "Je voulais encore te remercier pour ce que tu as fait pour moi."
    
    "Il sort de sa poche un melon pan un peu écrasé, mais je suis touché par son geste. Je crois que c’est la première fois que quelqu’un m’offre à manger."

    Moi "Oh…merci, c’est gentil."
    Akio "Je peux m’asseoir avec toi ?"

    "Je hausse les épaules. "

    Moi "Je ne pense pas que les autres reviendront maintenant donc vas-y."
    
    "Nous commençons alors à discuter des événements de la veille, de cette brute qui s’en prend à plus faible que lui sans impunité."
    "Je remarque toutefois qu’Akio n’est pas aussi enjoué que je le pensais."

    Akio "Dis…Je veux pas t’embêter, mais…"

    "Je le coupe immédiatement."

    Moi "Tu ne m’embêtes pas."

    "Akio rougit, lui donnant un air encore plus adorable et innocent. "
    Akio "En fait…Hibari m’a volé mon sac. Je ne voulais pas te le dire hier parce que tu as déjà beaucoup fait pour moi…"

    "Il baisse la tête, l’air honteux."

    Akio "...mais il y a beaucoup de choses qui comptent pour moi à l’intérieur. Ce n’est pas de l’argent, mais il y a des photos, des souvenirs, mes notes pour mes cours du soir."
    Akio "Senpai, je suis vraiment désolé de te demander ça, mais il me fait vraiment trop peur. Je sais que toi tu es fort…alors est-ce que tu pourrais récupérer mes affaires dans son casier ?"
    
    menu:
        "Aider Akio à récupérer ses affaires dans le casier de Takuya ?"
        "Lui dire d'aller voir un adulte":
            Moi "C’est compliqué pour moi Akio. En tant qu’élève, je ne suis pas censé pouvoir ouvrir un casier qui ne m’appartient pas…"
            
            hide akio normal
            show akio triste
            
            "Des larmes perlent au bord de ses yeux. Je peux voir à quel point il se retient de pleurer."
            "Je me sens si gêné de ne pas pouvoir l’aider !"
            Moi "Tu pourrais en parler à un professeur ou un surveillant ?"

            Akio "Je l’ai déjà fait, mais personne ne m’écoute. S’il-te-plaît, aide-moi senpai !"
            
            menu:
                "Aider Akio à récupérer ses affaires dans le casier de Takuya ?"
                "Accepter de l'aider":
                    hide akio triste
                    show akio normal
                    jump vengeance1
                "Refuser de l'aider":
                    Moi "Je suis désolé Akio, mais je viens à peine d’arriver dans ce lycée. Je ne veux pas m’attirer trop d’ennuis, tu comprends ?"
                    "Akio semble déçu, mais aucune larme ne coule finalement."
                    Akio "D'accord..."
                    "Il se lève et soupire." 
                    "Je tente de le retenir, de lui dire qu’on peut toujours être ami, mais rien n’y fait."
                    hide akio triste
                    
                    "Il quitte la salle et le poids de la culpabilité pèse sur mes épaules"
                    "Je passe l'après-midi à penser à cette histoire et je ne parviens pas à me concentrer sur autre chose."
                    scene kitchen
                    "Lorsque je rentre chez moi, je jette agressivement mon sac au sol et m’effondre par terre."
                    "C’est le moment que choisit le melon pan qu’Akio m’a offert, pour rouler hors de mon sac. "
                    "Quelle ironie."
                    "Je prends le gâteau recouvert d’un film plastique et le fait tourner entre mes mains. "
                    "Je remarque qu’il a dépassé sa date limite de consommation depuis plus d’un mois."
                    "Akio…"
                    "On ne se connait quasiment pas et pourtant je ne peux pas m’empêcher de penser à lui, de m’inquiéter pour lui."
                    "Est-ce que…j’aurais fait le mauvais choix ?"
                    "Je retire l'emballage de la friandise et croque agressivement dedans."
                    "Et si j'avais raté la seule occasion de me faire un véritable ami ?"
                    "Ah...Il faut que je lui présente mes excuses demain."
                    "Epuisé par cette journée, je finis par m’endormir par terre."

                    
                    
                    jump badend1Akio
        
        "Accepter de l'aider":
            jump vengeance1



    return
    
label vengeance1 :
    hide akio normal
    show akio heureux
    
    Moi "Bon, d’accord…je vais t’aider. Mais ça reste entre nous d’accord ?"

    "Akio semble retrouver le moral tout à coup."

    Akio "Merciiii !! Senpai !!"
    
    Moi "Attends-moi à 17h devant le portail d’accord ? Je te ramènerai tes affaires."
    Akio "Merci, merci senpai !"
    
    "Dans quoi est-ce que je viens de m’engager là ? Je parle comme si j’étais soudainement devenu expert en crochetage de casier."
    "Mais il a l’air si heureux que je n’ai pas le coeur à rétropédaler sur ma proposition."

    Akio "Tiens, voilà mon numéro. Comme ça on pourra s’envoyer des messages…Enfin, si tu veux bien sûr…"
    $ numberAkio = True

    "Je rougis. Je crois que c’est la première fois que je vais avoir le numéro de quelqu’un de ma classe. Il faut dire que d’habitude, je ne restais pas assez longtemps au même endroit pour apprendre à les connaître."
    
    Moi "O-ok ! Je t’ai envoyé un sms pour que tu puisses avoir mon numéro aussi."

    Akio "Il faut que j’y aille senpai. A tout à l’heure !"
    
    hide akio heureux

    "Akio quitte la salle et quelques minutes plus tard, mes camarades de classe reviennent de la pause déjeuner. Je ne pensais pas avoir discuté si longtemps avec mon kouhai !"
    "Je fais un rapide aller-retour au distributeur pour m’acheter des chips et les manger avant le début du cours de l’après-midi."
    "La littérature japonaise n’étant pas vraiment ma passion, je passe l’après-midi à élaborer un plan pour récupérer les affaires d’Akio."
    "A l’interclasse, Akio m’envoie d’ailleurs un message."

    "{b}Senpai ! C’est Akio :3 \n
        Je me suis renseigné pour le casier ! C’est le numéro 42. Hibari est un étudiant à l’université donc il finit les cours à 15h30. {/b}"
    
    "{b}Nous, c’est 15h15 donc tu auras une dizaine de minutes pour agir. \n
        Merci, merci, merci !!! A tout à l’heure ! :3.{/b}"

    
    "Je passe le reste du cours discrètement sur mon téléphone à échanger des textos avec Akio et chercher sur internet une manière de crocheter un cadenas."
    
    "{b}Driiiiiiiiiing{/b}"

    "Je me lève la boule au ventre, prends mon sac et marche rapidement en direction du bâtiment universitaire."
    
    scene hall 
    
    "J'ai un peu de mal à m'orienter au début, mais finalement les panneaux me mènent à destination."
    "Mes lectures m’ont appris que les cadenas de casier sont assez faciles à crocheter et qu’un trombone suffisait, que même un débutant pouvait y parvenir."
    scene lockers
    "Arrivé devant le casier 42 , je prends une grande inspiration."
    
    call screen consignes
    $ ok = _return
    if ok == True :
        jump quickgame3
    
    return

label quickgame3 :
    
    $ cont = 1 #continue variable
    $ arr_keys = ["a","z","e"] 
    $ i = 0
    while cont == 1 and i < 3:
        call qte_setup(0.5, 0.5, 0.01, renpy.random.choice(arr_keys), renpy.random.randint(1, 9) * 0.1, renpy.random.randint(1, 9) * 0.1) from _call_qte_setup_2
        # to repeat the qte events until it is missed
        $ i=i+1
        "{b}%(i)s{/b}"
    if cont == 0 :
        jump failQTElock #échec
    else :
        jump successQTElock #réussite
    
    
    return
    
label failQTElock :
    "Bon sang, j'ai beau essayer, je n'y arrive pas !"
    "Le temps passe à une vitesse folle et je commence à entendre des gens qui arrivent depuis le couloir."
    "Je décide d’en rester là avant de me faire prendre. Tant pis, je repars bredouille."
    
    scene bg street day
    "Sur le chemin du retour, j’envoie un message à Akio l’informer que je n’ai pas réussi et ne pas le faire attendre pour rien."
    "Je vérifie plusieurs fois mon téléphone sur la route, mais je ne reçois pas de réponse de sa part."
    "J’imagine que je l’ai déçu…"
    
    scene kitchen
    "Lorsque je rentre chez moi, je jette mon sac par terre et m’effondre par terre."
    "C’est le moment que choisit le melon pan qu’Akio m’a offert, pour rouler hors de mon sac. "
    "Quelle ironie."
    "Je prends le gâteau recouvert d’un film plastique et le fait tourner entre mes mains. "
    "Je remarque qu’il a dépassé sa date limite de consommation depuis plus d’un mois."
    "Akio…"
    "On ne se connait quasiment pas et pourtant je ne peux pas m’empêcher de penser à lui, de m’inquiéter pour lui."
    "Est-ce que…j’aurais dû persister un peu plus ?"
    "Je retire l'emballage de la friandise et croque agressivement dedans."
    "Et si j'avais raté la seule occasion de me faire un véritable ami ?"
    "Ah...Il faut que je lui présente mes excuses demain."
    "Epuisé par cette journée, je finis par m’endormir par terre."

    
    jump badend1Akio
    return
    
    
label badend1Akio :
    scene black
    show text "{color=FFF}Le lendemain{/color}"
    $ renpy.pause()
    scene kitchen
    
    "Je me réveille encore chamboulé par les événements de la veille."
    "Je ne pense qu’à une seule chose : m’excuser auprès d’Akio et espérer que l’on puisse encore devenir amis."
    scene bg street day
    "Sur la route, je m’arrête au konbini et achète des viennoiseries à partager avec lui pour me faire pardonner."
    
    scene hall
    "En cherchant la salle des 2-B au lycée, je me rends rapidement compte que quelque chose cloche."
    
    show randomStudent
    show randomStudent at tt_left
    show randomStudent2 at center
    
    "Eleve" "Tu as entendu ce qui est arrivé à Kimura ?"
    "Eleve 2" "Le petit blond timide ? Non…Enfin, Nozomi-kun m’a dit qu’il s’était battu avec un étudiant..."
    "Eleve" "Oui. Apparemment Hibari le rackettait et Kimura a voulu récupérer ses affaires, mais ça a mal tourné."
    "Eleve 2" "Oh, non, c’est horrible. Surtout par Hibari, il a dû le massacrer…"
    "Eleve" "Apparemment il est à l’hôpital. Ses parents ont dû venir en urgence depuis Los Angeles"
    "Eleve 2" "Pauvre Kimura-kun…"
    
    hide randomStudent
    hide randomStudent2
    
    "Je laisse tomber le sac que je tenais entre mes mains et reste paralysé par l’angoisse."
    "Akio…"
    "J’aurais dû le protéger."

    scene black
    show text "{color=FFF}Quelques semaines plus tard{/color}"
    $ renpy.pause()
    
    scene school 
    
    "La vie a repris comme si de rien n’était à l’académie Keimoo."
    "Comme si Akio Kimura n’avait jamais fréquenté cette école."
    
    if numberAkio==True :
        "J’ai essayé de lui envoyer des messages d’excuses et de l’appeler, mais il ne m’a jamais répondu."
        "Depuis 1 semaine, son numéro n’est plus attribué."
    else :
        "J’ai demandé à ses camarades de classe, personne n’avait son numéro pour prendre de ses nouvelles."
        "Malgré ses airs avenants, il n’avait pas vraiment d’amis. Il devait se sentir si seul et je n’ai pas vu sa détresse."
        
    "Takuya Hibari, lui, a enfin été expulsé de l’académie."
    "On raconte qu’il est dans des affaires louches à Bougu maintenant. Je ne chercherai pas à savoir si c’est la vérité."

    "Dans ma classe, je me suis fait quelques amis, mais je ressens toujours de la culpabilité."
    "Si j’avais été plus déterminé, peut-être qu’Akio mangerait avec nous à la cafétéria, qu’il jouerait avec nous à la salle d’arcade après les cours."

    "Ah, si seulement la vie avait un bouton pour sauvegarder aux moments critiques."



    
    scene black
    show text "{color=FF0000}BAD END{/color}" at text_effect
    $ renpy.pause()

    return
    

label successQTElock :
    "Le casier s’ouvre ! J’ai réussi ! C’était vraiment trop facile !"
    "Alors que je récupère le sac d’Akio, je me dis que je pourrais aller encore plus loin."
    "Est-ce que Takuya ne cherchera pas à se venger quand il découvrira quand j’ai ouvert son casier ?"
    "Peut-être que finalement, ça ne fera que mettre Akio plus en danger…"
    "Et si je parvenais à le faire exclure pour qu’il le laisse enfin tranquille ?"
    "Boosté par l’adrénaline, j’ai soudainement une idée : je pourrais laisser ma montre dans son casier, alerter un adulte et le faire expulser pour vol. Après tout, c’est une montre de valeur, ils ne pourront pas fermer les yeux comme pour les affaires d’Akio…"

    menu:
        "Que dois-je faire ?"
        "Laisser la montre dans le casier et venger Akio":
            "Ca lui fera les pieds à cette grosse brute."
            "J’enlève la montre de mon poignet pour la glisser entre les affaires d’Hibari, referme le casier et repart vers l’extérieur du bâtiment."
            "J’envoie un message à Akio pour l’informer de mon succès."
            $vengeance1 = True
            $goodEndAkio=goodEndAkio+1

        "Ne prendre que les affaires d'Akio":
            "Non, ce n’est pas raisonnable. Je ne veux pas être quelqu’un comme lui."
            "Je referme le casier et repart vers l’extérieur du bâtiment."
            "J’envoie un message à Akio pour l’informer de mon succès."

    scene school
    show akio heureux at center
    "Quelques minutes plus tard, je retrouve Akio au portail pour lui rendre son sac. Le blond semble radieux."
    Akio "Senpai !! Je le savais !! Tu es trop fort hihi."
    Moi "Ce n’est rien. Je ne pouvais pas te laisser comme ça sans rien faire."

    Akio "Tu sais…Tu es la première personne qui fait autant pour moi."

    "Je sens mon coeur s’emballer. Comment les autres ont pu laisser quelqu’un comme lui se faire maltraiter ? Il me fait de la peine."
    Akio "Ca te dit de venir avec moi à la cafétéria ? Je t’offre ce que tu veux !"

    if vengeance1 == True :
        Moi "J’ai encore quelque chose à faire là, mais demain je suis libre à midi !"
        Akio "Ok ! Je t’attendrai devant ta salle de cours !"
        
        hide akio heureux

        "Je retourne vers le bâtiment du lycée pour chercher un surveillant et lui parler de ma montre ‘volée’. Au loin, Akio continue de me faire de grands signes jusqu’à ce que je sois à l’intérieur."
        scene hall
        "Il est vraiment trop…kawaii !"
        "Je secoue ma tête. Je ne devrais pas penser à ça."
        "Où en étais-je déjà ? "
        
        show hayden normal at center
        Hayden "%(mainchar)s-kun ? Tout va bien ?"
        
        Moi "Oh, Yoshida-sensei. Je cherchais un surveillant."

        "Mais un prof fera aussi l’affaire finalement."
        "Je lui explique toute ma version de l’histoire : la bagarre avec Takuya Hibari qui tourne mal, il accepte de me laisser partir en échange de ma montre et je l’ai vu la mettre dans son casier."

        Hayden "Je vais prévenir la vie scolaire et son casier sera vidé ce soir. Si nous retrouvons votre montre, nous vous ferons convoquer chez le directeur."
        Moi "Merci professeur. Je tiens vraiment à cette montre, c’est un cadeau de mes parents."
        Hayden "C’est mon travail. Essayez de ramener une photo de vous avec votre montre pour prouver qu’elle vous appartient. Je m’occuperai du reste."

        "Je remercie une nouvelle fois mon enseignant puis décide de rentrer chez moi."
        hide hayden normal
        scene room
        "Quelle journée !"
        "Arrivé chez moi, je monte directement dans ma chambre pour m’allonger sur mon lit."
        "Je sors de mon sac le melon pan qu’Akio m’a offert ce midi. Je ne sais pas pourquoi, mais j’ai l’impression que c’est le meilleur que je n’ai jamais goûté de toute ma vie."
        "Je passe une partie de la soirée à envoyer des messages à Akio, oubliant un temps la solitude pesante de cette maison."
        "Je finis par m'endormir, épuisé, en serrant mon téléphone contre moi."

    else :
        Moi "Carrément !"

        "J’ai soudainement retrouvé l’appétit que je n’avais pas ces derniers temps. Il faut croire que c’est motivant de manger avec quelqu’un ?"
        "Nous passons la fin de l'après-midi ensemble à rire, manger, discuter de tout et de rien."
        
        hide akio heureux
        "Nous nous séparons finalement un peu avant 18h pour rentrer chez nous."
        
        scene room
        "Arrivé chez moi, je monte directement dans ma chambre pour m’allonger sur mon lit."
        "Je sors de mon sac le melon pan qu’Akio m’a offert ce midi. Je ne sais pas pourquoi, mais j’ai l’impression que c’est le meilleur que je n’ai jamais goûté de toute ma vie."
        "Je passe une partie de la soirée à envoyer des messages à Akio, oubliant un temps la solitude pesante de cette maison."
        "Je finis par m'endormir, épuisé, en serrant mon téléphone contre moi."
        
    jump day3Akio

    return
    
    
label day3Akio :
    
    scene black
    show text "{color=FFF}Quelques jours plus tard - 16/04/2018{/color}"
    $ renpy.pause()
    
    scene bg street day
    
    "Quelques temps ont passé depuis ma rentrée en 3ème année à Keimoo."
    "Mes parents auraient dû rentrer ce soir, mais ils m’ont envoyé un message pour m’annoncer qu’ils ne peuvent pas quitter leur bureau à l’étranger pour le moment."
    "Apparemment leur mutation pour le Japon prendra plus de temps que prévu, des dossiers doivent être traités en urgence…"
    "Je ne sais pas si je peux encore les croire, mais ce n’est pas comme si mon opinion comptait pour eux."
    "Sur une note plus positive, la brute qui a agressé Akio le premier jour de cours a été expulsée temporairement."

    if vengeance1 == True :
        "Mon plan a fonctionné, la vie scolaire a retrouvé ma montre dans le casier d’Hibari."
        "Il a longuement protesté, mais le directeur n’a rien voulu entendre. J’ai cru comprendre qu’il avait déjà été soupçonné de vol quand il était lycéen."
        "Finalement, je n’ai fait que rendre justice aux victimes et je ne me sens donc absolument pas coupable."
        "Un mois d'expulsion, ce n’est pas si cher payé pour tous ses mauvais comportements."
        "Et puis grâce à ça, Akio a retrouvé le sourire."
        
    else :
        "On aurait apparemment retrouvé un couteau dans son casier, ça craint…"
        "Je trouve ça un peu bizarre de ne pas avoir vu le couteau quand je suis allé récupérer les affaires d’Akio, mais je ne vais pas me plaindre de ne plus le voir."
        
    "Au moins, nous sommes un peu plus tranquilles pour le moment."
    
    scene school
    show akio normal at center

    Akio "Senpai, ohayooo ~ !"

    "Comme à son habitude depuis quelques jours, Akio m’attend devant le portail du lycée pour discuter de tout et de rien avant de rejoindre notre classe."
    "Je suis vraiment content d’avoir fait sa rencontre, c’est un garçon tellement gentil."
    "J’aurais aimé naître un an plus tard pour être dans sa classe plutôt que la mienne, mais bon c’est comme ça."
    
    Akio "Au fait senpai, ce soir les 2ème année ont une réunion d’orientation donc je ne pourrai pas te voir après les cours."
    Moi "Oh…D’accord. C’est pas grave."
    Akio "Mais on peut manger ensemble à midi, je t’achèterai un melon pan pour me faire pardonner !"
    Moi "Non, tu n’as pas besoin de m’offrir à manger ! Je mange avec toi parce que ça me fait plaisir."

    "{b}Driiiiiiiiiing{/b}"
    
    hide akio normal
    
    "La sonnerie retentit et nous devons nous séparer."
    
    scene classroom
    
    "Je rejoins ma salle de classe avec un pincement au coeur."
    "Lorsque j’arrive devant la porte, plusieurs personnes sont déjà à l’intérieur, mais pas le prof."
    "Je rejoins mon bureau en passant discrètement entre les élèves pour ne pas les déranger."

    show mei normal at center
    
    Mei "Ah, %(mainchar)s-kun, tu es là bonjour !"

    "La déléguée m’adresse un rapide signe de main et se dirige vers moi."
    "Je ne sais pas trop pourquoi est-ce qu’elle s’acharne à me parler tous les jours alors que je n’ai rien demandé."
    "Et puis, je n’aime pas comment son groupe d’amis nous regarde quand elle interrompt leur discussion pour venir me parler."
    
    Mei "Tu as passé un bon week-end ?"
    
    
    menu:
        Mei "Tu as passé un bon week-end ?"
        "Oui":
            Moi "Oui ça va."
            Mei "Ah, cool ! J’espère aussi que tu t’adaptes bien aux cours et à l’académie. N’hésite pas si tu as besoin qu’on te prête nos notes de l’an passé !"
            
        "Non":
            Moi "Non, pas vraiment."
            Mei "Ah…Oh…Désolée. Si on peut t’aider pour les cours ou autre chose, n’hésite pas à nous demander nos notes de l’an passé !"
        "Ne pas répondre":
            "Je détourne la tête et fait comme si elle n’existait pas jusqu’à ce qu’elle se décide à partir d’elle-même."
            
    "Par chance, la prof d’anglais arrive avant que la discussion n’aille plus loin."
    
    hide mei normal
    
    "Mei repart ainsi s’asseoir près de ses amis, non sans me lancer un regard intrigué."
    "Je parie qu’elle essaie de se rapprocher de moi pour me tirer les vers du nez sur ma famille et se moquer de moi avec son groupe d’hypocrites."
    "Pff…"
    "Je passe le reste du cours à ruminer. "
    "Si Akio était dans ma classe, peut-être que Mei arrêterait de me prendre en pitié parce que je n’ai pas d’ami ici."
    
    menu:
        "N’ayant pas le coeur à travailler, je décide de chercher une source de distraction."
        "Dessiner sur la feuille de note":
            "Je commence par dessiner sur un coin de ma feuille, puis dans toute la marge."
        "Envoyer des textos à Akio pour se plaindre":
            "Je déverse toute ma frustration par message à Akio, tant pis s’il ne pourra pas me répondre tout de suite."
        "Dormir":
            "Je pose ma tête sur mes avant-bras et décide de dormir, peu importe ce qu’en diront les autres."
            
    scene school
    
    "Lorsque la sonnerie retentit, je me sens soulagé de pouvoir quitter la salle et rejoindre Akio à la cafétéria."
    "Nos discussions me donnent la sensation d’une bouffée d’air pur."
    "Malheureusement, la pause repas est de courte durée et je dois rapidement retourner en classe où je trouve M.Yamamoto pour un cours d’histoire."

    scene classroom
    show yamamoto at center
    
    Yamamoto "Pour le mois prochain, j’aimerais que vous réalisiez une présentation en binôme pour aborder les différentes ères de la période contemporaine de l’histoire du Japon."
    Yamamoto "Composez les binômes et je vous attribuerai à chacun un sujet."
    
    "C’est bien ma veine."
    "Autour de moi, les groupes se forment et les tables bougent. Avec un peu de chance, le prof me laissera faire le travail seul."
    
    Yamamoto "Ah, %(mainchar)s-san. Vous êtes seul ? Tanaka-san aussi. Pourriez-vous vous mettre ensemble s’il-vous-plaît ?"
    
    hide yamamoto

    "N’ayant pas encore mémorisé tous les noms de mes camarades de classe, je regarde autour de moi jusqu’à croiser le regard de l’autre élève."
    show naoko normal
    "Ah, oui, c’est cette fille. Il faut dire qu’elle est assez discrète donc je ne l’ai pas beaucoup remarquée."
    "Il faut aussi dire qu’elle n’a pas l’air très commode."
    "Je me déplace donc afin de me mettre à côté d’elle pendant que le prof commence à distribuer les sujets."
    
    "Arrive enfin notre tour…"
    show naoko normal at tt_left
    show yamamoto at center

    Yamamoto "Il ne reste plus que…l’économie pendant ère Showa. Vous avez de la chance, c’est passionnant."
    
    "Il retourne ensuite devant le tableau et nous donne des consignes plus précises sur ce qui est attendu de la présentation."
    hide yamamoto
    show naoko normal at tt_right
    "J’observe du coin de l’oeil Tanaka-san prendre des notes jusqu’à ce que le prof nous dise de commencer."
    
    Moi "..."

    "Je ne sais pas comment engager la discussion et Tanaka-san n’a pas vraiment l’air bavarde non plus."
    
    show naoko normal at center

    Moi "Tanaka-san ? Tu as une euh…idée par quoi on commence ?"
    
    "Elle hausse les épaules, l’air indifférente."

    Naoko "On peut se répartir les tâches ?"

    
    menu:
        "Proposer de faire le diapo":
            Moi "Je veux bien faire le diapo. L’histoire c’est pas trop mon truc alors si je peux éviter tout ce qui est recherche…"
        "Proposer d'aller faire des recherches à la bibliothèque":
            Moi "Je veux bien aller chercher des livres et faire des recherches à la bibliothèque. Je suis nul en informatique alors si je pouvais éviter le diapo…"

    "Elle hoche la tête, l’air toujours relativement nonchalant."

    Naoko "D’accord. On peut faire un plan chacun de notre côté si tu veux. Et on mettra en commun avant la fin du cours."
    Moi "Ok ! Faisons ça."
    
    hide naoko normal
    
    "Nous commençons à travailler tous les deux en silence alors que d’autres groupes profitent du travail en binôme pour parler fort."
    "Le temps en temps, je jette un coup d’oeil en direction de Tanaka-san qui n’a pas l’air dérangée par le brouhaha."
    "Les minutes défilent et je trouve enfin la force de me concentrer sur le devoir…"
    "…"
    
    show naoko normal at center
    
    Naoko "%(mainchar)s-san ? J’ai terminé."

    "Nous profitons des 15 dernières minutes du cours pour faire un plan qui nous satisfait tous les deux."
    "La sonnerie retentit et je retourne à ma place pour ranger mes affaires."
    "Alors que plusieurs élèves restent à leur table pour discuter, j’observe Tanaka-san quitter la classe seule."
    
    hide naoko normal
    
    "Je me demande si elle se sent aussi exclue, marginalisée par le reste du groupe. Peut-être que je devrais essayer de devenir son ami ?"
    
    
    if cuisine == True :
        "Puisqu’on est aussi dans le même club, peut-être que je devrais lui dire de se mettre avec Akio et moi la prochaine fois ?"
        "…"
        "Enfin, je ne sais pas si c’est une bonne idée."
        "Si Akio finissait par se rapprocher plus d’elle que de moi, je me retrouverais encore une fois tout seul."
        "Les choses sont bien comme elles sont, finalement."
        
    else :
        "Je pourrais essayer de lui parler quand j’attends Akio à la sortie du club de cuisine, je crois qu’ils sont ensemble."
        "Je me demande s’ils se sont déjà parlés ou si…ils sont proches ?"
        "Je commence à regretter de ne pas m’être inscrit au club de cuisine en début d’année, j’aurais pu passer plus de temps avec Akio."
        
    
    scene bg street day
    
    "Je passe rapidement au konbini après les cours pour remplir un peu le frigo."
    "Mes parents ne seront pas là pour encore au moins une semaine alors je ne m’attarde pas trop sur les produits frais."
    "Quelques plats préparés, des oeufs, des ramens instantanés et des melon pan devraient faire l’affaire."
    
    scene kitchen

    "Je finis par rentrer chez moi épuisé, comme d’habitude." 
    "Je range les courses et me prépare une coupe de ramen instantanés que je mange devant la télévision."
    "Je guette mon téléphone de temps à autre, mais je ne reçois aucun message d’Akio. Je me dis que sa réunion a dû s’éterniser."
    "Une fois la nuit tombée, je monte à l’étage me brosser les dents puis me coucher."
    
    scene room
    
    "Je suis un peu triste de ne pas avoir pu raconter mon après-midi à Akio, mais je ne veux pas non plus trop l’embêter avec mes problèmes."
    "J’espère qu’on pourra se voir demain, et tous les jours d’après."


    
    return