﻿# The script of the game goes in this file.

define audio.start = "music/oregairu.mp3"

# Declare characters used by this game. The color argument colorizes the
# name of the character.

#dates
define Moi = Character("[mainchar]", who_color="#4d4dff")
define Akio = Character("Akio", who_color="#00b33c")
define Takuya = Character("Takuya",who_color="#FF0000")
define Hayden = Character("Hayden",who_color="#228B22")
define Mei = Character("Mei",who_color="#8B008B")

#others
define Hisaka = Character("Hisaka")
define Haruhiko = Character("Haruhiko")
define Cammy = Character("Cammy")
define Naoko = Character("Naoko")
define Ayame = Character("Ayame")
define Arata = Character("Arata")

define Yamamoto = Character("M.Yamamoto")

#characters sprites

#akio
image akio normal = im.Scale("akio/akio normal.png", 400, 500)
image akio triste = im.Scale("akio/akio triste.png", 400, 500)
image akio apathique = im.Scale("akio/akio apathique.png", 400, 500)
image akio heureux = im.Scale("akio/akio happy.png", 400, 500)

#takuya
image takuya normal = im.Scale("takuya/takuya normal.png", 400, 500)

#hayden
image hayden normal = im.Scale("hayden/hayden normal.png",400,500)
image hayden embarrassed = im.Scale("hayden/hayden embarrassed.png",400,500)

#mei
image mei normal = im.Scale("mei/mei normal.png",400,500)
image mei embarrassed = im.Scale("mei/mei embarrassed.png",400,500)
image mei heureuse = im.Scale("mei/mei heureuse.png",400,500)
image mei colere = im.Scale("mei/mei colere.png",400,500)

#naoko
image naoko normal = im.Scale("naoko/naoko normal.png",400,500)

#hisaka
image hisaka normal = im.Scale("hisaka/hisaka normal.png",400,500)
image hisaka embarrassed = im.Scale("hisaka/hisaka embarrassed.png",400,500)

#arata
image arata normal = im.Scale("arata/arata normal.png",350,700)
image arata colere = im.Scale("arata/arata colere.png",350,700)

#papa
image papa normal :
    im.Scale("papa/papa normal.png",400,1200)

#maman
image maman normal = im.Scale("maman/maman normal.png",250,700)
image maman colere = im.Scale("maman/maman colere.png",250,700)

#lock

image locker = im.Scale("locked/locked.png",25,25)

#others
image librarian = im.Scale("other/librarian.png",400,500)
image randomStudent = im.Scale("other/randomStudent.png",400,500)
image randomStudent2 = im.Scale("other/randomStudent2.png",400,500)
image yamamoto = im.Scale("other/yamamoto.png",400,500)


init -1:
    transform tt_left:
        xpos 500    
        easein 0.5 xpos 200    
            
    transform tt_right:
        xpos 200    
        easein 0.5 xpos 600    
    
    transform text_effect:
        parallel:
            block:
                linear 0.1 xoffset -2 yoffset 2 
                linear 0.1 xoffset 3 yoffset -3 
                linear 0.1 xoffset 2 yoffset -2
                linear 0.1 xoffset -3 yoffset 3
                linear 0.1 xoffset 0 yoffset 0
                repeat
        parallel:
            block:
                alpha .2
                linear 1.0 alpha .9
                linear 1.0 alpha .2
                repeat
'''
"Function"/pseudo-function
calls the qte screen
parameters are:
    - amount of time given
    - total amount of time (is usually the same as above)
    - timer decreasing interval
    - the key/keyboard input to hit in the quick time event
    - the x alignment of the bar/box
    - the y alignment of the bar/box
'''
label qte_setup(time_start, time_max, interval, trigger_key, x_align, y_align):

    $ time_start = time_start
    $ time_max = time_max
    $ interval = interval
    $ trigger_key = trigger_key
    $ x_align = x_align
    $ y_align = y_align

    call screen qte_keyboard
    # can change to call screen qte_button to switch to button mode

    $ cont = _return
    # 1 if key was hit in time, 0 if key not

    return
    

label start:
    stop music fadeout 1.0
    play music start
    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.


    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.


    # These display lines of dialogue.
    
    $ bagarre = False
    
    scene black
    show text "{color=#fff}Prologue - 02/04/2018{/color}"
    $ renpy.pause()
    
    
    scene school
    
    "Un nouveau jour, et encore un nouveau lycée."
    
    "A cause du travail de mes parents, je me retrouve à déménager de nouveau"
    
    "Et me voilà, encore, à devoir tout recommencer depuis le début."
    
    "C'est la rentrée et j'intègre cette fois lycée de l'académie Keimoo."
    
    "C'est un établissement renommé, un campus géant, et franchement ça fait rêver."
    
    "Espérons juste que cette fois, je ne sois pas obligé de partir au bout d'un mois."
    
    "{b}8h45, entrée du lycée.{/b}"
    
    "Il y a déjà tellement d'élèves"
    
    "Ils se bousculent tous pour apercevoir le tableau d'affichage."
    
    "Essayons  de nous approcher des listes."
    
    menu:
     "Que dois-je faire ?"
     "Pousser les autres élèves":
         "Faites place, je passe !"
         "J'entends quelques grognements sur mon passage, mais je vais les ignorer."
         "J'ai beau essayer de me frayer un chemin, la tâche est plus ardue que prévue"
         "..."
         "Enfin, j'arrive au panneau d'affichage."
         
     "Attendre mon tour":
        "Je décide de patienter sagement."
        "Les minutes passent, mais la foule ne s'amoindrit pas. Regardons ma montre."
        "Si ça continue, je vais finir par être en retard"
        "Ah, je vois une ouverture. Allons-y."
        

    "Je repère vite mon nom parmis les lignes. 3ème année, classe B"
    "Chouette, mais ça me dit absoluement pas où c'est."
    "Je m'extrais avec difficulté de la masse étudiante et cherche du regard un plan, des indications ou une quelconque aide divine pouvant me mettre sur la voie de ma salle de classe."
    "Rien."
    "Nothing."
    "Nada."
    "Cette journée commence bien."
    
    menu:
     "Que dois-je faire ?"
     "Abandonner et rentrer chez moi":
         jump firstend
     "Chercher dans le hall":
         scene hall
     "Chercher dans les jardins":
         scene garden
     "Demander son chemin":
         scene school
         
    "Tiens, il y a cette fille à côté de l'entrée..."
    "Elle connait peut-être mieux l'endroit que moi."
    
    "Moi" "Euh...Excusez-moi ?"
    
    show mei normal
    
    "???" "Je peux t'aider ?"
    "Moi" "Oui...euh je suis nouveau, je cherche la 3-B."
    "???" "*sourit* Ah ! Ca tombe bien, j'y vais moi aussi. Je peux te montrer le chemin si tu veux."
    
    menu:
        "Accepter":
            "Moi" "Ah, merci avec plaisir !"
            "La jeune fille passe devant et me fait signe."
            "Je la suis à travers les couloirs et en quelques instants, nous voilà devant la salle de classe."
            "Elle entre en première, et après une courte hésitation, je lui emboîte le pas."
            hide mei normal
        "Refuser":
            "???" "Oh, d'accord. On se voit plus tard du coup..."
            "Je ne sais pas trop pourquoi j'ai refusé, mais maintenant qu'elle est partie, je vais vraiment devoir me débrouiller tout seul."
            "Je tourne à droite, puis à gauche dans le bâtiment principal..."
            "Je finis par trouver une signalétique, et me voilà enfin devant ma classe."
            "J'inspire, et entre."
    
    scene classroom
    "La plupart des élèves sont déjà là, et le brouhaha règne."
    "Je sens les regards se tourner vers moi."
    "Apparemment, je suis le seul nouveau. Génial."
    
    show mei normal
    "La fille de tout à l'heure est assise au premier rang. Elle me fait signe."
    
    "???" "Du coup, je ne te l'ai pas dit, mais on sera camarades pour cette année."
    "???" "Au fait, je m'appelle Mei Shiozaki, mais tu peux m'appeler Mei !"
    
    python :
        mainchar = renpy.input("Enchanté...Mei. Moi c'est ...")
        if not mainchar :
            mainchar = "Yusuke"
    
    Mei "Enchantée de même, %(mainchar)s !"
    
    hide mei normal
    
    "{b}Quelqu'un se racle la gorge.{/b}"
    
    show hayden normal
    
    "En me retournant, j'aperçois un homme qui doit être notre professeur."
    "Il a l'air assez jeune, j'espère qu'il est sympa."
    
    "Professeur" "Bonjour tout le monde, bonne rentrée. Pour ceux qui ne me connaissent pas, je suis Yoshida-sensei."
    "Professeur" "Je serai votre professeur principal pour cette année"
    
    "Il semblerait que la nouvelle ravisse mes camarades puisque j'entends quelques gloussements admiratifs de la plupart des filles de la classe."
    "Yoshida-sensei ne semble pas perturbé et continue."
    
    Hayden "Comme vous avez pu le remarquer, nous accueillons un nouvel élève à Keimoo."
    Hayden "%(mainchar)s, tu veux bien te présenter ?"
    
    "Super, une présentation orale devant toute la classe, j'en rêvais !"
    "Je me racle la gorge et fais face à mes camarades."
    
    hide hayden normal
    
    menu:
        Moi "Bonjour, je m'appelle %(mainchar)s. J'ai 17 ans. L'année dernière j'étais dans un lycée.."
        "en France.":
            Moi "...en France."
        "en Chine.":
            Moi "...en Chine."
        "aux USA.":
            Moi "...aux USA."
        "en Russie.":
            Moi "...en Russie."
        "au Qatar.":
            Moi "...au Qatar."
        
    Moi "Mes parents sont japonais, mais ils travaillent beaucoup à l'étranger. On vient de s'installer à Keimoo."
    
    menu:
        Moi "Sinon dans la vie, j'aime bien..."
        "les jeux vidéos":
            Moi "...les jeux vidéos."
        "faire du sport" :
            Moi "...faire du sport."
        "le piano" :
            Moi "...le piano."
        "manger" :
            Moi "...manger."
        "la nature" :
            Moi "...la nature."
        "sortir avec mes amis":
            Moi "...sortir avec mes amis."
    
    Moi "Voilà...J'espère qu'on s'entendra bien."
    "Le calvaire est terminé. Je m'en suis pas trop mal sorti."
    
    
    show hayden normal
    
    Hayden "Très bien. Le principal a déjà dû te mettre au courant du fonctionnement de notre établissement, et notamment l'obligation de rejoindre un club."
    Hayden "Voici la liste de ceux qui sont ouverts cette année ainsi qu'un formulaire."
    Hayden "Tu devras me transmettre ton choix avant la fin de la journée. En attendant, tu peux aller t'asseoir à un bureau libre."
    
    hide hayden normal
    
    "Je scanne la salle du regard. Il y a plusieurs places disponibles."
    
    menu:
        "Je choisis de m'asseoir :"
        "au premier rang":
            "Au moins je verrai bien le tableau."
        "près de la fenêtre":
            "La vue a l'air d'être sympa dehors."
        "au dernier rang":
            "Au moins, personne ne me dérangera ici."
        "en plein milieu de la salle":
            "Je dois essayer de me faire des amis."
            
    "A mon bureau, je jette un oeil à la liste des clubs."
    "J'hésite entre plusieurs, mais mon choix se concentre sur trois en particulier."
    "Ce n'est pas une décision à prendre à la légère, après tout elle est irréversible (en tout cas pour un an) !"
    
    menu:
        "C'est décidé, je vais rejoindre le club de ..."
        "Cuisine":
            "Peut-être que je saurai me préparer mon bentô d'ici l'année prochaine !"
            $ cuisine = True
            $ natation = False
            $ musique = False
            $hayden_route = 0
        "Natation":
            "Faire du sport me fera le plus grand bien."
            $ natation = True
            $ cuisine = False
            $ musique = False
            
            $hayden_route = 1
        "Musique":
            "J'espère pouvoir m'améliorer au piano."
            $ musique = True
            $ natation = False
            $ cuisine = False
            $hayden_route = 0
            
    "Ce choix en moins sur la conscience, j'écoute le reste du cours pendant la matinée."
    "Au final, cela consiste surtout à expliquer comment va se passer l'année."
    
    "{b}Driiiiiiiing{/b}"
    
    "C'est la pause déjeuner !"
    "J'en profite pour rendre le formulaire à Yoshida-sensei."
    
    show hayden normal
    
    Hayden "Oh, %(mainchar)s. Tu t'es décidé rapidement ! Je vais transmettre tout ça aux dirigeants de clubs."
    
    if natation == True :
        Hayden "Ah, mais je vois que ce ne serait pas nécessaire en fait."
        Moi "Euh ?"
        Hayden "C'est moi qui encadre le club de natation. C'est un plaisir de te compter parmis nous."
        Hayden "Les séances ont lieu 2 fois par semaines. Je vais demander à Shiozaki-san de te faire parvenir l'emploi du temps."
        
        "Shiozaki...c'est cette fille, Mei ? Elle fait partie du club apparemment."
        "Elle a l'air responsable. Même les profs lui font confiance."
        Hayden "Bon, il faut que j'aille en salle des profs."
    
    Hayden "Pour l'instant j'ai une réunion, on se revoit plus tard %(mainchar)s."
    
    hide hayden normal
    
    "Enfin c'est pas tout, mais j'ai faim. Elle est où la cafétéria ici ?"
    "C'est un labyrinthe ce lycée !"
    
    scene garden
    
    "Me voilà perdu, encore."
    "Je voulais aller à la cafétéria, mais je me retrouve devant des sortes de jardins ?!"
    "Il faut vraiment que je trouve un plan de ce fichu campus..."
    
    "???" "NON ! AIE ! LAISSE-MOI !!!"
    
    "Je rêve ou quelqu'un est en train de se faire agresser ?"
    "Je m'approche de la source des cris tout en restant caché."
    "J'aperçois un petit blond plaqué au sol par un grand mec baraqué qui n'a pas l'air commode."
    
    show akio normal
    show akio normal at tt_left
    show takuya normal at center
    
    "Brute" "File-moi ton fric ou je te fais taire vite fait, bien fait."
    "Victime" "*pleure* J-je j'ai rien sur moi, me fais pas de mal s'il-te-plaît...*sniff"
    "Brute" "Déconne pas. Vous êtes tous des fils de bourges ici. Quelques milliers de yens en moins, ça va pas vous manquer."
    "Victime" "Je jure que c'est vrai !"
    
    "L'agresseur ne semble pas convaincu et s'apprête à donner un coup de poing au blondinet."
    
    menu:
        "Que dois-je faire ?"
        "Aider la victime":
            jump akioRoute1
        "Ne rien faire et s'en aller":
            jump takuyaRoute1
        "Aller chercher de l'aide":
            "Je ne peux pas affronter cette situation seul. Il faut que j'aille chercher de l'aide...Mais qui ?"
            menu:
                "Qui appeler à l'aide ?"
                "Le professeur principal":
                    "Yoshida-sensei devrait pouvoir m'aider."
                    if natation == True :
                        $hayden_route = 2
                        jump haydenRoute1
                    else :
                        "Merde...J'ai oublié ce qu'il m'a dit ce matin. Il doit être rentré chez lui."
                        "Je n'ai pas le temps d'attendre plus longtemps. La première personne sur qui je tombe fera l'affaire."
                        jump meiDefaultRoute2
                "La première personne qui me tombe sous la main":
                    "Je n'ai pas le temps d'attendre plus longtemps. La première personne sur qui je tombe fera l'affaire."
                    jump meiDefaultRoute2

    return

label firstend :
    scene black
    show text "{color=#fff}Tu veux jouer le jeu ou non ? {/color}"
    $ renpy.pause()
    return
    


###AKIO'S ROUTE###

label akioRoute1 :
    "Je ne peux pas rester les bras croisés. Il faut que j'intervienne !"
    Moi "HEY ! Lâche-le !"
    "La brute a l'air surprise que quelqu'un se mêle de cette affaire. C'est mal me connaître !"
    "Il lâche le blond et fait craquer ses doigts d'un air menaçant."
    "Je me suis peut-être mis dans la merde en fait..."
    
    "Brute" "Tu dois être nouveau, apparemment tu sais pas ce que ça fait de s'opposer à Takuya Hibari."
    Takuya "Crois-moi...Je vais vite t'apprendre."
    
    "..."
    
    call screen consignes
    $ ok = _return
    if ok == True :
        jump quickgame1
        
label quickgame1 :
    
    $ cont = 1 #continue variable
    $ arr_keys = ["a","z","e"] 
    $ i = 0
    while cont == 1 and i < 5:
        call qte_setup(0.5, 0.5, 0.01, renpy.random.choice(arr_keys), renpy.random.randint(1, 9) * 0.1, renpy.random.randint(1, 9) * 0.1) from _call_qte_setup
        # to repeat the qte events until it is missed
        $ i=i+1
        "{b}%(i)s{/b}"
    if cont == 0 :
        jump meiDefaultRoute1
    else :
        jump akioRoute2
    return
    

label akioRoute2 :
    hide takuya normal
    show akio normal at tt_right
    "Malheureusement pour ce Takuya, je suis rapide et j'ai réussi à esquiver son premier coup."
    "Sans perdre de temps, je m'enfuis en attrapant au passage le blond pour nous réfugier dans un endroit fréquenté."
    "Il n'osera pas nous tabasser en public. Enfin j'espère."
    
    "Victime" "*essoufflé* Senpai...tu m'as sauvé...Merci !"
    Moi "De rien...Mais c'est pas passé loin. Il est fou ce mec ! Ca va, tu n'es pas blessé ?"
    "Victime" "Non.. Tu es arrivé juste à temps. Je pourrais jamais te remercier assez…"
    Moi "T'en fais pas. Si tu tiens à tout prix à me dédommager, tu peux juste me dire où est la cafétéria, ça m'aiderait beaucoup !"
    "Victime" "*sourit* Sans problème. Au fait, je ne connais pas le nom de mon sauveur.."
    Moi "Oh, moi c'est %(mainchar)s."
    "Victime" "Je m’appelle Akio Kimura, je suis en 2-B, et plus tard je serais un chirurgien renommé."
    Moi "C'est un sacré projet, bon courage !"
    Akio "Hihi"
    
    "Akio me mène jusqu'à la cafétéria et nous nous séparons. Il me fait des grands gestes avec un sourire. Difficile à croire qu'il était en larmes il y a à peine 5 minutes."
    
    hide akio normal
    
    "Finalement tout est bien qui finit bien."
    "Akio a l'air d'être vraiment gentil. Peut-être que je devrais l'inviter à manger à la cafétéria un jour ?"
    "Je n'ai jamais eu de vrai ami à cause de mes déménagements. Peut-être que c'est enfin l'occasion de me rapprocher de quelqu'un."
    "Maintenant que j'y suis, je me rappelle que j'ai toujours détesté manger seul le midi. C'est comme si les autres me regardaient en me jugeant."
    "J'hésite à rester du coup."
    "..."
    "Je devrais acheter un melon pan et visiter un peu le campus. J'ai entendu dire qu'il y avait une super salle de musique. Avec un peu de chance, j'y serai tranquille..."
    
    scene kitchen
    
    "J'arrive chez moi."
    "Comme d'habitude, la maison est vide. J'ai passé l'après-midi à m'entraîner au piano à l'académie."
    "J'ai été dérangé quelques fois, mais globalement ça a été. Personne n'est venu me harceler ou me demander de partir parce que je gênais."
    "Il commence à se faire tard et j'ai de nouveau faim. Après tout, je n'ai pas mangé grand chose à midi."
    "La vision de la cuisine sombre et vide ne me motive pas vraiment."
    "J'ouvre le frigo et constate qu'il n'y a pas grand chose à l'intérieur vu que mes dernières courses remontent il y a 2 semaines."
    "Tant pis pour ce soir, j'ai assez eu d'émotion pour la journée, et vais directement me coucher."
    
    scene room
    
    "J'espérais recevoir un appel de mes parents, mais mon téléphone reste désespérément silencieux."
    "Ils doivent être trop occupés par leur travail à l'étranger pour penser à moi."
    "Sur ces pensées déprimantes, je m'endors, les yeux rivés sur mon portable..."
    
    
    
    scene black
    show text "{color=#fff}Route d'Akio{/color}"
    $ renpy.pause()
    jump routeAkiobegin
    return


###MEI'S ROUTE###

label meiDefaultRoute1 :
    
    $bagarre = True

    
    hide akio normal
    "Même si j'ai vu venir le coup, je n'ai pu ni l'esquiver, ni contre attaquer..."
    "Me voilà au sol, à me tenir le ventre, et ce fameux Takuya fouille dans mon sac pour en sortir mon portefeuille."
    "Il se sert sans gêne et me le jette dessus avant de se retourner vers le blond."
    
    show akio normal
    show akio normal at tt_left
    
    Takuya "Ton pote paye ta part pour aujourd'hui, mais la prochaine fois..."
    Takuya "T'as intérêt à coopérer sagement."
    
    "Les mains dans les poches, il repart comme si de rien n'était."
    hide takuya normal
    show akio normal at tt_right
    
    "Son ancienne victime se précipite vers moi en pleurs."
    
    "Victime" "Tu vas bien senpai ? Désolé, c'est ma faute s'il t'a frappé...!"
    Moi "Ca fait un mal de chien ! Si j'avais su, je ne serais pas intervenu."
    "Victime" "Je suis désolé, si je peux faire quelque chose pour me racheter..."
    Moi "Non ça va, je vais juste partir loin d'ici."
    "Victime" "Sniff. Si jamais je peux me faire pardonner, tu peux me trouver en 2-B et demander Akio Kimura. Je ferai n'importe quoi, senpai."
    Moi "Ouais, merci..."
    
    hide akio normal
    
    "Je me relève difficilement et titube vers les allées centrales du jardin."
    "Quelle poisse, avec tout ça, j'ai même pas encore mangé !"
    
    "Je suis dans un sale état moi maintenant. Je doute qu'on me laisse rentrer dans la cafétéria et que je puisse manger sans m'attirer des regards."
    "Je devrais probablement abandonner l'idée de manger ici et rentrer chez moi."
    "On ne me reprendra plus à jouer au héros. La prochaine fois, je demanderai mon chemin à Mei au lieu de foncer tête baissée."
    
    scene kitchen
    
    "J'arrive chez moi."
    "Heureusement que mes parents ne sont pas encore rentrés. Je les remercie intérieurement de m'avoir laissé du curry d'hier soir au frigo."
    "Le repas me réconforte un peu, mais mes égratignures me piquent toujours."
    "Je débarrase, puis décide de me changer et de panser mes plaies."
    
    scene room
    
    "Je passe le reste de l'après-midi à me détendre sur l'ordinateur, traîner sur les réseaux sociaux ou jouer à des jeux en ligne."
    "Ce n'est que lorsque j'entends la porte d'entrée s'ouvrir que je comprends que mes parents sont de retour."
    "J'hésite un instant à sortir de ma chambre."
    
    "Papa" "%(mainchar)s ? Tu es là ?"
    
    "Ne pouvant pas esquiver plus longtemps, je sors de ma cachette."
    
    scene kitchen
    show papa normal at tt_left
    show maman normal at center
    
    "Mes parents constatent rapidement les quelques pansements sur mes bras et mon visage. Ils accourent pour m'inspecter."
    
    "Maman" "Qu'est-ce qu'il s'est passé ? Comment tu t'es fait tout ça ?"
    "Papa" "Ca va %(mainchar)s ? Tu as mal quelque part ? Est-ce que tu veux qu'on aille voir le médecin ?"
    
    "Je ris légèrement pour tenter de calmer leur inquiétude."
    
    Moi "Je vais bien, ce sont juste quelques égratignures. J'ai vu une brute aujourd'hui à l'académie et j'ai voulu m'interposer."
    Moi "Je me suis fait ça en tombant quand il m'a bousculé."
    
    "Ma réponse n'a pas l'air de les rassurer."
    
    "Papa" "Une brute ? Pour ton premier jour ? Tu veux qu'on appelle le proviseur demain ?"
    "Maman" "Est-ce qu'il y a eu des témoins ? Il ne faudrait pas que ça se reproduise, même si c'est avec un autre élève."
    
    Moi "Ne vous inquiétez pas, j'en parlerai à mon professeur principal demain. J'essaierai aussi de faire attention et de l'éviter."
    "Papa" "C'était courageux de vouloir t'interposer, mais il faut parfois éviter les ennuis."
    "Papa" "Ta mère et moi voulons le meilleur pour toi et surtout, que tu ne cours pas de danger."
    "Papa" "Téléphone-nous au siège si cet émergumène s'approche un peu trop de toi une nouvelle fois."
    
    "Mes parents me prennent dans leurs bras et je me sens soulagé d'avoir pu leur en parler."
    "Ma mère se redresse et m'offre un grand sourire malicieux."
    
    "Maman" "En tout cas, j'espère que tu as faim ! Ton père et moi n'avions pas très envie de cuisiner donc nous avons commandé à emporter."
    "Maman" "Tu aimes bien la cuisine française je crois ?"
    
    "Nous nous mettons à table en discutant. Mes parents me racontent leur journée, et moi la mienne, en évitant de rementionner la dérouillée que je me suis prise."
    "Je leur parle plutôt de mes nouveaux camarades, mes professeurs et la grandeur du campus."
    
    "Epuisés de leur journée, mes parents vont se coucher juste après le dîner."
    
    hide papa normal
    hide maman normal

    
    "J'en profite pour regarder un film à la télé, puis finit par aller, moi aussi, me coucher."
    
    scene black
    show text "{color=#fff}Route de Mei{/color}"
    $ renpy.pause()
    
    jump routeMeibegin
    
    return

label meiDefaultRoute2 :
    hide akio normal
    hide takuya normal
    show naoko normal
    
    $bagarre = False
    
    "Cette fille, je l'ai déjà vue quelque part, elle ne serait pas dans ma classe ?"
    "Je lui fais signe de s'arrêter et elle s'exécute."
    "Arrivé à sa hauteur, je reprends mon souffle pour lui parler."
    
    Moi "Excuse-moi de te déranger, mais il y a un sale type. Grand, fort, qui s'en prend à un petit blond."
    Moi "Je...je ne pense pas que je ferai le poids contre lui. Est-ce que tu sais ce qu'on pourrait faire pour l'aider."
    
    "???" "Petit blond..."
    
    "Elle semble réfléchir un instant, puis son visage s'éclaircit, comme si elle avait compris quelque chose."
    
    "???" "Où sont-ils ?"
    
    menu:
        "Où se déroule la bagarre ?"
        "A l'entrée des jardins":
            Moi "A l'entrée des jardins. Je m'y suis perdu tout à l'heure."
        "Dans les serres du club de jardinage":
            Moi "Dans les serres du club de jardinage. Je m'y suis perdu tout à l'heure."
        "Dans la cafétéria":
            Moi "Dans la cafétéria."
            
    Moi "Attends, tu vas y aller toute seule ?"
    
    "Elle ne prend pas le temps de me répondre et se précipite vers l'endroit que je lui ai indiqué."
    
    hide naoko normal
    
    "Etudiant A" "Tiens, pourquoi Naoko va aux jardins ? Elle a changé de club ?"
    "Etudiant B" "Aucune idée."
    
    "Naoko...Je devrais la remercier plus tard."
    
    "J'espère qu'elle va s'en sortir toute seule..."
    "Désolé petit blond, c'était trop pour moi. Vu ma corpulence, je n'aurais pas fait long feu face à la brute."
    "Je vais rentrer chez moi avant que la brute ne me retrouve. Si ça se trouve, je suis le prochain sur sa liste."
    "Tant pis pour la cafétéria, je demanderai à Mei demain."
    
    scene kitchen
    
    "J'arrive chez moi."
    "Mes parents ne sont pas encore rentrés, mais ils m'ont laissé du curry d'hier soir au frigo."
    "Ca me remet du baume au coeur après cette perturbation."
    "Une fois repu, je décide alors de m'occuper durant le reste de l'après-midi."
    
    scene room
    
    "J'allume mon ordinateur et je ne vois pas le temps passer, entre réseaux sociaux, blogs et jeux en ligne."
    "Ce n'est que lorsque j'entends la porte d'entrée s'ouvrir que je comprends que mes parents sont de retour."
    "Je me précipite pour les saluer."
    
    scene kitchen
    show papa normal at tt_left
    show maman normal at center
    
    "Papa" "Nous sommes rentrés %(mainchar)s !"
    Moi "Vous avez passé une bonne journée ? Le travail s'est bien passé ?"
    "Maman" "Fatigant, comme d'habitude. Ton père et moi n'avions pas très envie de rentrer pour cuisiner."
    "Maman" "Du coup, nous avons commandé à emporter. J'espère que tu as envie de cuisine française."
    
    "Ma mère m'embrasse sur la tête en passant et nous nous mettons à table."
    "Je leur raconte mon premier jour, leur parle de mes camarades et mes nouveaux professeurs."
    "Epuisés de leur journée, mes parents vont se coucher juste après le dîner."
    
    hide papa normal
    hide maman normal
    
    "J'en profite pour regarder un film à la télé, puis finit par aller, moi aussi, me coucher."
    
    scene black
    show text "{color=#fff}Route de Mei{/color}"
    $ renpy.pause()
    
    jump routeMeibegin
        

###TAKUYA'S ROUTE###

label takuyaRoute1 :
    hide akio normal
    hide takuya normal
    "Je tourne les talons, je ne veux pas être mêlé à ce genre d'histoire, surtout le premier jour."
    "C'est triste pour ce blondinet, mais c'est pas mes affaires."
    
    "???" "Hey, t'as cru que tu pouvais te casser en scred comme ça ?"
    show takuya normal
    menu:
        "Et merde, la brute m'a rattrapé..."
        "L'ignorer":
            "Je passe mon chemin et quitte au plus vite cette conversation."
            "Oof."
            "Malheureusement, il ne m'en laisse pas l'occasion et me plaque contre un mur."
        "Lui répondre":
            Moi "Il me semble qu'on est dans un pays libre ici ?"
            "Apparemment ça n'a pas l'air de lui plaire."
            "Il m'attrape par le col et me plaque au mur."
            
    "Brute" "Ecoute ptit con, je sais pas pour qui tu te prends, mais ici on apprend à respecter Takuya Hibari si on veut vivre longtemps."
    Takuya "Capiche ?"
    
    menu:
        "Que dois-je faire ?"
        "L'ignorer":
            "Je reste silencieux, ça ne sert pas à grand chose de donner du grain à moudre à des gens comme lui."
            "Au pire, il va s'énerver, se défouler et je serai tranquille plus vite."
            "C'est pas la première fois qu'une brute croit pouvoir faire la loi juste parce qu'il fait deux têtes de plus que tout le monde."
            Takuya "Heh. Pour un nouveau t'as du cran de t'opposer à moi."
            Takuya "Mais je te préviens, si tu fais ta poucave, je le saurais..."
            Takuya "Et tu vas morfler."
            "Il me relâche violemment, mais contre toute attente, je ne me suis pas fait casser la gueule."
            "Au moins une chose qui se passe bien aujourd'hui, wouhou !"
            
        "Le provoquer":
            Moi "T'es qui toi ? Le caïd du lycée ? Juste parce que tu rackettes des avortons, tu penses que tu vas gagner mon respect ? J'en ai vu d'autres !"
            "Je m'attends à recevoir un coup, mais il ne vient pas."
            Takuya "Un bourge qui joue les durs, intéressant."
            Takuya "Mais j'te préviens, si tu fais ta poucave, je le saurais et tu vas morfler."
            Takuya "J'te fermerais ta grande gueule avec plaisir, nabot."
            
            "Il me relâche violemment, mais contre contre toute attente, je ne me suis pas fait casser la gueule."
            "Faut croire que ce Takuya grogne fort, mais ne mord pas !"
            "Bah, de toute façon, c'est pas comme si j'allais rester longtemps dans ce lycée."
            "J'peux bien me mettre à dos une racaille ou deux."
    
    hide takuya normal
    
    "Bon c'est pas tout, mais il faudrait peut-être songer à manger."
    "Par chance, ma fuite m'aura mené pas loin de la cafétéria. Je prends place dans la queue et attends mon tour."
    "La tentation de dépasser quelques élèves qui n'avancent pas assez vite à mon goût est grande, mais je parviens à me contrôler."
    "Je finis par commander des tamagoyakis et manger seul dans mon coin en écoutant de la musique."
    "La routine du nouveau quoi."
    "Le temps passe et finalement...13h15."
    "La cafétéria se vide. L'idée de rentrer dès maintenant ne me tente pas vraiment, et si je passais à la salle d'arcade sur le chemin ?."
    
    "..."
    
    scene kitchen
    
    "J'arrive chez moi."
    "Il est 17h et je sais que ma mère m'attend de pied ferme. A peine j'ouvre la porte qu'elle se précipite à l'entrée."
    
    show maman colere at center
    
    "Mère" "%(mainchar)s  ! Tu as vu l'heure ? Où étais-tu ? Tu n'avais cours que jusqu'à midi et je t'avais dit de rentrer directement !"
    "Mère" "Je me suis fait un sang d’encre ! J’ai appelé ton lycée, il m’ont dit que tu n’y étais pas et que tu avais quitter l’enceinte de l’établissement après le repas !"
    "Mère" "J'attends ton explication."
    
    Moi "Je suis allé en ville, pas la peine d'en faire toute une histoire."
    
    "Mère" "Une histoire ?! Tu te rend compte qu’il aurait pu t’arriver quelque chose ?! Que des gens mal intentionnés aurait pu s’en prendre à toi."
    "Mère" "Attends que ton père rentre et tu t'expliqueras avec lui."
    
    "Je me tais et espère pouvoir m'éclipser dans ma chambre, mais ma mère m’arrête dans mon élan."
    
    "Mère" "Où vas-tu comme ça ? Je te rappelle que tu n'as pas encore révisé ton piano. Ton nouveau professeur particulier viendra dès la semaine prochaine."
    "Mère" "J'attends de toi une rigueur exemplaire."
    
    hide maman colere
    
    "Je pose mon sac dans l’entrée et m’installe au piano à contre coeur."
    "Je ne peux même pas espérer souffler puisque ma mère me surveille depuis la cuisine et veille à ce que j’effectue tous mes exercices."
    "Je déteste le piano, mais mes parents insistent pour que je continue à pratiquer. J’aurais préféré faire de la guitare mais ils ne veulent rien entendre."
    "Ils ne veulent jamais rien entendre."
    
    "..."
    
    "Deux longues heures s’écoulent au rythme du métronome, jusqu’à ce que j’entende la porte de l’entrée s’ouvrir."
    "Je me fige à l’entente des pas de mon père, et de ceux de ma mère qui se précipite pour l’accueillir et le débarrasser de sa veste."
    "Je les entends faiblement parler de moi dans le couloir."
    "Je reprends mes exercices de gammes pour tenter de couvrir leurs voix et être le plus exemplaire possible."
    show papa normal at tt_left
    "Mon père me rejoint calmement dans le salon et pose une main sur le piano. Je m’arrête de jouer, les doigts crispés sur les touches."
    
    Moi "Bonsoir père."
    
    "Avant même de dire un mot, sa paume claque sur ma joue d’une gifle nette et rapide. Je baisse la tête, silencieux."
    "Comme si de rien était, il fait demi-tour et s’installe à la table de la salle à manger où tout est déjà prêt."
    show maman normal
    "Je ravale mes larmes et le suit pour en faire de même."
    "Ma mère nous sert le repas, et je mange en silence tandis qu’elle raconte des banalités sur sa journée et sur le voisinage à mon père."
    
    "..."
    
    "Le dîner se termine, mon père quitte la table pour travailler dans son bureau et ma mère commence la vaisselle."
    hide papa normal
    
    "Mère" "%(mainchar)s, tu as des devoirs ?"
    Moi "Non...Les professeurs ont dit qu'ils attendraient demain pour nous en donner."
    "Mère" "Montre-moi ton agenda."
    
    "Je m'exécute."
    
    "Mère" "Très bien. Dans ce cas là, tu peux aller lire tes manuels dans ta chambre. Je viendrai te dire quand tu pourras te coucher."
    hide maman normal
    "La douleur encore présente sur ma joue me convainc de ne pas faire d'histoire."
    
    scene black
    show text "{color=#fff}Route de Takuya{/color}"
    $ renpy.pause()
    
    jump routeTakuyabegin
    
    return

###HAYDEN'S ROUTE###

label haydenRoute1 :

    "Il a dit qu'il serait en réunion. Il faut que je trouve la salle des profs !"
    "Je cours aussi vite que je peux en direction du bâtiment principal quand...Miracle !"
    scene school
    show hayden normal
    
    "C'est Yoshida-sensei !"
     
    
     
    "On dirait bien qu'il est sorti prendre une pause pour fumer."
     
    Moi "Sensei !"
     
    Hayden "Oh, %(mainchar)s. Tu as l'air paniqué. Tout se passe bien ?"
     
     
     
    menu:
        Moi "Un étudiant s'est fait agresser. Il se trouve..."
        "A l'entrée des jardins.":
            $hayden_route = 3
            Moi "...à l'entrée des jardins. Je m'y suis perdu et je les ai trouvés."
        "A la cafétéria":
            Moi "...à la cafétéria. Ils ont commencé à se battre alors que j'allais chercher à manger."
        "Dans le gymnase":
            Moi "...dans le gymnase. Je visitais le campus et je les ai trouvés."
             
    Hayden "Quoi ? Déjà ..."
    
    "Le prof prend un air grave et range sa cigarette dans son cendrier de poche."
    
    Hayden "Merci et désolé que tu vois ça dès le premier jour... Tu as vu l'agresseur ?"
    
    menu:
        Moi "Oui, c'est un grand ..."
        "Brun avec un uniforme du lycée et une barbe":
            Moi "...brun avec un uniforme du lycée et une barbe. Il fait vieux, mais je pense qu'il doit être en 4ème année."
        "Blond avec un uniforme du lycée":
            Moi "...blond avec un uniforme du lycée. On dirait un kôhai."
        "Brun habillé en civil avec une barbe":
            $hayden_route = hayden_route +1
            Moi "...brun habillé en civil avec une barbe. Il fait nettement plus vieux qu'un lycéen."
        "Blond habillé en civil":
            Moi "...blond habillé en civil."
     
    menu:
        Moi "Je crois qu'il voulait..."
        "Faire taire la victime au sujet d'un délit":
            Moi "...faire taire la victime à propos d'un délit dont la victime a été témoin."
        "Obliger la victime à voler de l'argent pour lui":
            Moi "...obliger la victime à racketter d'autres étudiants pour lui."
        "Se venger de la victime":
            Moi "...se venger de la victime, mais je ne sais pas pour quoi exactement."
        "Voler le téléphone de la victime":
            Moi "...voler le téléphone de la victime pour le revendre en ville."
        "Voler l'argent de la victime":
            $hayden_route = hayden_route +1
            Moi "...voler l'argent de la victime."
        "Voler le vélo de la victime":
            Moi "...voler le vélo de la victime pour le revendre en ville."
            
    if hayden_route == 5 :
        Hayden "Takuya Hibari...Ce n'est pas la première fois qu'il fait ça."
        Hayden "Il faut que je prévienne l'université que ça ne peut plus durer. Encore merci pour ton aide. On se revoit demain !"
        
        
        Moi "Takuya...Il va falloir que je fasse attention à lui."
    
    else :
        Hayden "J'espère ne pas arriver trop tard car je n'arrive pas à identifier l'agresseur avec la description que tu en fais..."
        Hayden "Encore merci pour ton aide. On se revoit demain !"
        
        
        Moi "J'espère que je ne me suis pas trompé..."
        
    "Le prof esquisse ce qui semble être un sourire de façade avant de courir dans la direction que je lui ai indiquée."
    hide hayden normal
    
    "Je me laisse glisser contre le mur le plus proche et prends une grande inspiration."
    "C'était une sacrée journée."
    "Avec tout ça, je n'ai même pas encore mangé."
    "Vu l'heure, il ne doit plus rester grand chose à la cafétéria. Et ce n'est pas comme si je savais où elle est."
    "Je demanderai à des gens de ma classe demain. En attendant, c'est peut-être l'occasion d'aller faire des courses et me préparer à manger."
    "Yosh, c'est parti pour le konbini."
    
    "..."
    
    scene kitchen
    
    "J'arrive chez moi."
    "Comme d'habitude, la maison est vide. Je ne perds pas de temps et me mets aux fourneaux."
    "J'ai une faim de loup et le repas ne va pas se faire tout seul !"
    "Je jette un oeil à l'horloge, il est bientôt 14h."
    "J'aimerais bien passer un coup de fil à mes parents, mais aux Etats-Unis il fait encore nuit."
    "J'attendrai quelques heures pour leur raconter mon premier jour."
    "J'en profiterai aussi pour leur demander quand ils pourront revenir de leur voyage d'affaire. J'ai hâte !"
    
    
    scene black
    show text "{color=#fff}Route d'Hayden{/color}"
    $ renpy.pause()
    
    jump routeHaydenbegin
    
    return
     
    
    
    
    
    
    
