label routeMeibegin :
    ###GLOBAL VARIABLES###
    
    $ super = 0 #Points pour la super good end de Mei - Il faut X points pour la débloquer

    $ number = False #donner son numéro à Mei

    $ date1 = False #Date 1 débloqué
    $ success_datemei = 0 #nombre de dates avec Mei réussis (détermine la fin)
    $ ico = False #Si on a montré l'info de sur l'icône lock au joueur


    #Premier cours de MC, exposé avec Mei pour le cours d'Hayden
    scene room
    
    if bagarre == True :
        "Les picotements sur ma joue me réveillent quelques minutes en avance sur mes prévisions, me rappelant la bagarre de la veille."
        "A peine debout, je file dans le salle de bain afin de prendre une douche et mettre un pansement sur ma plaie. A l’extérieur de la pièce, ma mère me demande de me dépêcher."
    else :
        "Bip...Bip...Bip."
        "Maudit réveil !"
        "Je me lève et me rue dans le salle de bain pour être le premier à l'occuper."
        "Je traîne un peu sous la douche, en ignorant soigneusement les cris de ma mère qui me demande de me dépêcher."
    
    scene kitchen
    
    "Lorsque j’arrive dans la cuisine, le petit-déjeuner est déjà sur la table et mon père prêt à partir au travail."
    "Nous nous saluons rapidement alors que j’engloutis un bol de riz en quelques minutes."
    
    scene room
    
    "De retour dans ma chambre, j’enfile des vêtements frais, attrape mon sac préparé la veille et quitte la maison en trombe."
    
    scene school
    
    "Heureusement, je n’habite pas très loin du lycée et j’arrive à l’heure à mon premier vrai cours à Keimoo ! "
    
    scene classroom
    
    "Quelques élèves sont déjà présents dans la salle et discutent avec entrain."
    "Pour ma part, je me fais discret et reprends la même place que la veille. "
    "J’adresse quelques sourires timides à mes voisins, mais je n’ose pas engager de conversation."
    
    show hayden normal
    
    Hayden "Bonjour à tous !"
    
    "Je suis un peu soulagé lorsque Yoshida-sensei débarque car je me sens moins seul ainsi."
    "Le prof fait l'appel, puis la séance débute."
    
    hide hayden normal
    
    menu:
        "C'est parti pour 3h30 de mathématiques."
        "Prendre des notes":
            "J’ai toujours été bon élève en mathématiques."
            "Je n’ai pas du mal à suivre ce que dit Yoshida-sensei et prends des notes."
            "Je suis si concentré que je ne vois pas le temps passer."
            
        "Regarder dehors":
            "Les mathématiques ne m’ont jamais passionnées."
            "Je décroche au bout de quelques minutes à peine et laisse mon regard vagabonder vers l’extérieur où d’autres élèves font des tours de terrain pour le cours de sport."
            "Le prof ne semble pas vraiment remarquer que j’aie la tête ailleurs et le cours devient un peu plus supportable dans ces conditions."
            
        "Dormir":
            "Je ne résiste pas longtemps à l’envie de bâiller, puis celle de poser ma tête sur mes bras et enfin celle de fermer les yeux. "
            "La voix mélodieuse de Yoshida-sensei expliquant la résolution des équations du second degré se fait de plus en plus lointaine, et je finis par m’endormir."
            "....ZZzzZ…."
            "Lorsque je reprends connaissance, plusieurs élèves ont le regard tourné vers moi avec l’air amusé, mais pas le prof."
            "Je me redresse et fais semblant de prendre des notes durant le reste de l’heure."

    "Un quart d’heure avant la fin du cours, alors que toute la classe prie pour être dispensée de devoirs, Yoshida-sensei nous annonce une bien douloureuse nouvelle."
    
    show hayden normal
    
    Hayden "C’est plutôt inhabituel en cours de maths, mais j’ai pensé que faire exposé serait une bonne idée pour permettre aux plus moyens d’entre vous d’avoir la moyenne ce trimestre."
    
    "Des protestations fusent dans la salle, mais Yoshida-sensei ne cède pas."
    
    Hayden "Pour la semaine prochaine, je veux que chacun me soumette un sujet d’exposé sur une application des mathématiques dans un métier. "
    Hayden "Ce travail sera présenté en binôme pour le début du mois de mai. Vous avez jusqu’à la sonnerie pour composer les groupes."
    
    hide hayden normal
    
    "Comprenant que les négociations avec Yoshida-sensei ne sont plus permises, les autres élèves se mettent en quête d’un binôme."
    "Ne connaissant personne, je lance plusieurs regards hésitants autour de moi, sans bouger de ma chaise. "
    
    show mei normal
    
    Mei "On peut se mettre ensemble si tu veux."
    "Je sursaute car je n’avais pas remarqué la présence de la fille qui me sourit."
    
    menu:
        "Que dois-je répondre ?"
        "Oui ! C'est gentil de te proposer.":
            Mei "Super ! Je vais dire à Yoshida-sensei qu'on fera ce travail ensemble !"
            "Ouf, me voilà casé pour cet exposé ! Mei m'a l'air d'être une fille sérieuse. On devrait avoir une bonne note en combinant nos efforts."
            "Je profite du temps qu’il reste en classe pour commencer à réfléchir à l’exposé dans mon coin. "
            "La cloche finit par sonner quelques minutes plus tard, annonçant la fin des cours de la matinée."
            "Mei m’adresse un signe de main amical et disparaît rapidement dans l'entrebâillement de la porte."
            
        "Non, je préfère être seul":
            Mei "Oh...Je vois."
            "Elle a l’air déçue, mais je n’aime pas recevoir la pitié des autres."
            
            hide mei normal
            
            "Amer, je détourne le regard tandis qu’elle se dirige vers le bureau du prof."
            
            show mei embarrassed
            show mei embarrassed at tt_left
            show hayden embarrassed at center
            
            "Quelques instants plus tard, je fais face à Yoshida-sensei qui m’adresse un regard gêné."
            
            Hayden "%(mainchar)s-san,  je suis désolé mais ce devoir est à rendre en binôme. Mei-san étant la dernière élève encore seule à part toi, vous formerez le groupe 10."
            "Derrière lui, Mei me lance un sourire faussement enjoué qui disparaît au moment où Yoshida-sensei se dirige vers un autre binôme."
            hide hayden embarrased
            show mei embarrassed at tt_right
            Mei "Bon eh bien...On dirait bien qu’on est quand même ensemble."
            "Je ne réponds pas, ne sachant pas comment me sortir de cette situation embarrassante. "
            hide mei embarrassed
            "Heureusement, la cloche nous libère et nous rangeons nos affaires chacun de notre côté."
            
            
        "T'es qui déjà ?":
            hide mei normal
            show mei embarrassed
            Mei "Oh, euh...Je m'appelle Mei. Tu sais, on s'est déjà vus hier."
                
            menu:
                "Elle rougit, sans doute embarrassée par ma question."
                "S’excuser pour sa mauvaise mémoire et accepter de travailler avec elle":
                    Moi "Désolé, j’ai vraiment une mauvaise mémoire. C’est d’accord pour l’exposé, Mei !"
                    hide mei embarrassed
                    show mei normal
                    Mei "Super ! Je vais dire à Yoshida-sensei qu’on fera ce travail ensemble !"
                    hide mei normal
                    
                    "Ouf, me voilà casé pour cet exposé ! Mei m’a l’air d’être une fille sérieuse. On devrait avoir une bonne note en combinant nos efforts. "
                    "Je profite du temps qu’il reste en classe pour commencer à réfléchir à l’exposé dans mon coin."
                    "La cloche finit par sonner quelques minutes plus tard, annonçant la fin des cours de la matinée."
                    "Je range mes affaires et quitte la classe en silence."
                "Appeler Yoshida-sensei pour être avec quelqu'un d'autre":
                    "Bien sûr, je savais qui est cette fille, je voulais juste qu’elle s’en aille, mais elle n’a pas l’air de comprendre le message."
                    "Sans lui répondre, je lève la main et attends que Yoshida-sensei me donne la parole pour clamer haut et fort que je ne veux pas d’elle."
                    Moi "Sensei, j’aimerais être avec quelqu’un d’autre qu’elle."
                    "Le teint de Mei vire au rouge vif et je la regarde tourner les talons pour se rasseoir à sa place."
                    hide mei embarrassed
                    scene black
                    show text "{color=#FF0000}BAD END \n C'est un jeu de drague, fais un effort !{/color}"
                    $ renpy.pause()
            
    jump day1Part2
    
    return
    
label day1Part2 :
    #Lunch 1er jour + Mei propose rdv bibliothèque + soirée MC
    
    "C’est l’heure d’aller manger. Mon ventre gargouille d’impatience."
    
    scene hall
    
    "Je suis mes camarades de classe en direction de la cafétéria, ce serait dommage de me perdre encore une fois."
    
    
    "???" "Salut, euh..."
    show akio normal at center
    "Je fais volte-face et me retrouve nez-à-nez avec le garçon blond de la veille."
    
    if bagarre == True :
        Akio "Je voulais te remercier d'avoir pris ma défense hier."
    else :
        "Victime" "Je suis Akio Kimura de la 2B. On s’est...croisés hier, tu sais."
        Akio "Je voulais te remercier de m’avoir envoyé de l’aide hier."
        
    "Il rougit et fixe le sol. Le temps semble s’être arrêté. Tous les regards sont braqués sur nous. "
    Moi "Ce n’est rien, je ne pouvais pas te laisser te faire brutaliser."
    "Je fais un pas en arrière en espérant dissiper le malaise entre nous."
    Akio "Attends !!"
    
    "De plus en plus d’élèves s’arrêtent pour nous regarder. Je ne peux plus ignorer les chuchotements qui s’élèvent dans la foule."
    "Tétanisé, je ne trouve pas la force de répondre quoique ce soit."
    
    Akio " Senpai, je...J’AIMERAIS SORTIR AVEC TOI !"
    
    "Il a crié ces mots dans le couloir, redevenu silencieux, comme si tous les autres retenaient leur souffle en attendant ma réponse."
    "A ce moment, je n’ai qu’une envie : disparaître."
    
    menu :
        "Que répondre à cette déclaration enflammée ?"
        
        "On peut juste être amis s’il-te-plaît ?":
            hide akio normal
            show akio triste
            "La foule éclate de rire."
            hide akio triste
        "Non.":
            hide akio normal
            show akio apathique
            "La foule éclate de rire."
            hide akio apathique
    
    show akio triste
    "Blessé, Akio éclate en sanglots et part en courant, percutant au passage une de mes camarades de classe."
    show naoko normal
    show naoko normal at tt_left
    Akio "Désolé"
    Naoko "Pas grave."
    Naoko "Tu m'as pas l'air bien. Tu devrais aller à l'infirmerie."
    
    "La suite de leur conversation restera un mystère pour moi. Je finis par les perdre de vue lorsqu’ils disparaissent dans un couloir adjacent."
    
    hide akio triste
    hide naoko normal
    
    "Encore secoué par les événements, je reprends ma route vers la cafétéria où une longue queue m’attend."
    "L’attente est longue et je ne suis pas vraiment récompensé au bout du chemin."
    "Ce midi, je me contenterai de ce que les autres élèves n’ont pas voulu : un onigiri au thon et une salade d’algues."
    
    "Je prends place à côté d’un groupe d’élèves qui sont dans ma classe. Ils passent l’entièreté du repas à débattre de la meilleure équipe de baseball lycéenne, mais je ne les écoute pas vraiment."
    "Après la pause, nous retournons vers notre salle de classe.."
    
    scene classroom
    
    show mei normal
    
    "Mei, mon binôme pour l’exposé du cours de maths, m’attend devant la porte. Elle me sourit."
    
    Mei "Je voulais savoir si tu as déjà une idée pour ce que tu veux faire pour l’exposé."
    Mei "Pour rappel, il faut que l’on trouve des applications des mathématiques dans un métier de notre choix."
    
    menu :
        "Quel sujet d'exposé choisir ?"
        "Pourquoi pas l'architecture":
            Moi "J’aimerais bien savoir comment on applique ce qu’on voit en classe à la structure des bâtiments."
            Mei "C’est une bonne idée ! J’ai hâte de voir ce qu’on va découvrir"
        "L’astrophysique, ça a l’air sympa":
            Moi "Je m’intéresse beaucoup au ciel. Enfin, j’aime surtout regarder les étoiles."
            Mei "C’est une bonne idée ! Moi aussi j’aime le ciel !"
        "J’ai entendu dire qu’on faisait des statistiques en sociologie":
            Moi "Je me suis renseigné sur la fac de sociologie, c’est ce que j’aimerais faire après le lycée."
            Mei "C’est une bonne idée ! En plus, je pense que c’est plutôt original par rapport à ce que va faire le reste de la classe."
        "Non, je n'y ai pas réfléchi.":
            Moi "Désolé. J’ai eu des ennuis ce midi, je n’ai pas eu le temps d’y penser."
            Mei "Ce n’est rien ! Il ne t’est rien arrivé de grave j’espère ?"
            Moi "Non, ne t'en fais pas."
    
    "Mei se met à se triturer nerveusement les mains. Je lui lance un regard interrogateur."
    
    Mei "En fait, j’ai pensé que...si ça t’arrange, je peux faire le devoir toute seule."
    Mei "Je veux dire, je connais bien Yoshida-sensei et je sais ce qu’il attend donc si tu as besoin d’un peu de temps pour t’adapter…"
    Mei "Je peux faire les recherches et d’envoyer le diapo avec ce que tu devras présenter."
    
    menu:
        Mei "Qu'est-ce que tu en penses ?"
        "Ok, je te laisse faire.":
            $ number = False
            $ date1 = False
            Mei "Ok ! Eh bien, je t’enverrai tout ce dont tu auras besoin quand j’aurai terminé."
        "Non, je t'enverrai ma partie par mail.":
            $ number = False
            $ date1 = False
            Mei "D’accord ! Bon, ça aurait été plus simple de tout faire en ensemble, mais je peux m’adapter.."
        "Non, c’est un travail de groupe, je ne peux pas te laisser tout faire":
            $ date1 = True #on débloque le date 1
            show mei heureuse
            "Le sourire de Mei s’élargit. Elle semble approuver d’un signe de tête."
            Mei "J’espérais que tu dirais ça !"
            
            hide mei heureuse
            show mei normal
            
            menu:
                "Maintenant, il ne reste plus qu’à convenir d’une date pour travailler."
                "On peut bosser ce soir chez moi après les clubs ?":
                    $ number = False
                    Mei "Désolée, je ne suis pas libre ce soir. Mais ce week-end je peux ! La bibliothèque du lycée est ouverte samedi. Ca te dit ?"
                    "Je hoche la tête."
                    Mei "Super. Bon, voilà mon numéro."
                    "Elle sort son téléphone et compose une série de chiffres qu’elle me montre."
                    Mei "Comme ça, s’il y a un problème, tu n’auras qu’à m’appeler !"
                "On peut se voir à la bibliothèque demain après les cours ? Il n’y a pas d’activités de club.":
                    $ number = False
                    Mei "Désolée, j’ai déjà promis à mon copain que je passerai l’après-midi avec lui.  Mais ce week-end je peux ! La bibliothèque du lycée est ouverte samedi. Ca te dit ?"
                    "Je hoche la tête."
                    Mei "Super. Bon, voilà mon numéro."
                    "Elle sort son téléphone et compose une série de chiffres qu’elle me montre."
                "Je te donne mon numéro et on en discute ce soir ?":
                    $ number = True
                    Mei "Oui, d'accord !"
                    "Je sors mon téléphone et affiche mon numéro sur l’écran."
                    Mei "Super, c’est noté ! Je t’enverrai mes disponibilités ce soir."
    
    hide mei normal
    "Notre discussion nous aura coûté les dernières minutes de la pause de midi."
    "Le professeur de japonais, bien moins sympathique que Yoshida-sensei, nous rappelle les règles de sa classe ainsi que les livres à acheter avant de débuter le cours."
    "Les heures de cours de l’après-midi s’enchaînent ainsi, sans événement majeur à l’horizon."
    
    "{b}Driiiiiiiing{/b}"
    
    "La délivrance arrive à 15h où nous rejoignons chacun nos clubs respectifs."
    
    
    if natation == True :
        scene pool
        "Je fais connaissance avec quelques kouhais, mais cette première séance est surtout consacrée l’élection du nouveau président du club."
        "Je ne connais aucun des candidats alors ça ne m’intéresse pas vraiment."
        "Au premier rang, Mei m’adresse un signe de main. "
        "Elle est assise à côté d’un grand garçon brun un peu renfrogné que je ne connais pas."
        "J’imagine que c’est son copain."
        
        show hayden normal
        Hayden "J’ouvre les votes pour désigner le prochain capitaine du club !"
        hide hayden normal
        "Je quitte Mei des yeux pour rejoindre les autres élèves qui font la queue pour accéder à l’urne."
        
    elif cuisine == True :
        scene kitchen club
        "Je fais connaissance avec quelques kouhais, mais cette première séance est surtout consacrée l’élection du nouveau président du club."
        "Je ne connais aucun des candidats alors ça ne m’intéresse pas vraiment."
        "Dans la queue pour accéder à l’urne, je reconnais un certain garçon aux cheveux blonds qui semble en meilleure forme qu’à midi."
        "Il ne semble pas m’avoir vu."
        "Soulagé, je dépose mon papier dans la boîte en carton faisant office d’urne."
    else :
        scene music club
        "Je fais connaissance avec quelques kouhais, mais cette première séance est surtout consacrée l’élection du nouveau président du club."
        "Je ne connais aucun des candidats alors ça ne m’intéresse pas vraiment."
        
    
    menu:
        "J'arrive devant l'urne."
        "Voter blanc":
            "C'est la solution la plus honnête. Je glisse un papier vierge dans l'urne."
        "Voter au hasard.":
            "Peu importe qui est élu président de toute façon. J'écris un nom au hasard et glisse mon papier dans l'urne."
            
    "Et voilà, c'est chose faite."
    "Nous sommes libérés quelques instants après le dépouillement et je peux enfin rentrer chez moi."
    
    scene kitchen
    
    "Ouf ! Quelle journée !"
    
    if number == True :
        "Hein ? Qui peut bien m’envoyer un message."
        "Je déverrouille mon téléphone."
        "Oh...C’est Mei. J’avais oublié cette histoire d’exposé."
        
        "{b}Salut, c’est Mei ! \n
        Je suis disponible ce week-end pour travailler. \n
        On peut se rejoindre à la bibliothèque du lycée ? Elle est ouverte le samedi.{/b}"
        
        "Samedi...Je n’ai rien de prévu."
        "Je confirme ma présence par texto et range mon téléphone."

    "Mes parents m’ont prévenu qu’ils rentreront sûrement très tard ce soir alors j’ai la maison pour moi tout seul."
    "Je me sers copieusement dans le frigo où m’attend le délicieux bento préparé par maman."
    "Il n’y a pas à dire : sa cuisine est vraiment la meilleure !"
    
    $ devoir1 = False #Etat des devoirs de MC
    
    if date1 == False :
        $ ico = True
        call screen icone
    
    menu:
        "En mangeant, je réfléchis à ce que je pourrais bien faire ce soir."
        "Faire ses devoirs":
            $ devoir1 = True
            "Je sors mes cahiers et travaille en écoutant un mix lofi hip-hop toute la soirée."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "{image=locker} Lire le blog de Mei {image=locker} (disabled)" if date1 == False:
            "..."
        "Lire le blog de Mei" if date1 == True:
            "Je décide d’en apprendre un peu plus sur Mei. Après tout, nous sommes dans la même classe et nous allons faire un exposé ensemble."
            "Je trouve rapidement un de ses profils en ligne et je finis sur son blog où je lis les derniers billets."
            "J’y apprends sa passion pour les univers Ghibli et la natation, entre autre."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "Lire un manga":
            "Après une journée comme celle-ci, j’ai besoin d’un peu de détente."
            "Je me dirige vers ma bibliothèque et sort cinq tomes d’Eyeshield 21."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "Jouer aux jeux vidéo":
            "Après une journée comme celle-ci, j’ai besoin d’un peu de détente."
            "J’allume mon ordinateur et rejoins mes partenaires de jeu sur WoW."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "Faire du sport":
            "Faire du sport me permettra d’évacuer le stress de la journée."
            "Je me mets en quête d’un programme sportif sur internet et commence en douceur avec des exercices de musculation simples."
            "Je finis la soirée par une bonne douche chaude avant d’aller me coucher."
    
    if date1 == True :
        jump date1Mei
    else :
        jump missedDate1Mei
    return
    
label missedDate1Mei :
    scene black
    show text "{color=#fff}Date 1 raté{/color}"
    $ renpy.pause()
    
    jump monday1
    return


label date1Mei :
    
    scene black
    show text "{color=#fff}Date 1 : Bibliothèque - 07/04/2018{/color}"
    $ renpy.pause()
    
    scene library
    
    "Les jours suivants se montrèrent plus tranquilles que mes deux premiers au lycée Keimoo."
    "La fin de la semaine arrive rapidement, et je commence à prendre mes repères dans les bâtiments, grâce à l’aide de mes camarades de classe."
    "J’ai notamment sympathisé avec deux garçons : Hisaka Rika et Haruhiko Nakamura. Malheureusement, ils n’ont pas l’air de vraiment s’apprécier mutuellement, pour une raison que j’ignore."
    "Si j’avais osé leur parler mon premier jour, peut-être que j’aurais pu faire l’exposé de mathématiques avec l’un d’eux ?"
    "Même si Mei s’est toujours montrée sympa avec moi, je dois avouer que je suis un peu nerveux…"
    "Et je le suis d’autant plus qu’elle a déjà 5 minutes de retard à notre rendez-vous studieux à la bibliothèque."
    "Les minutes défilent et ma boule au ventre s’intensifie."
    "Et si elle m’avait oublié ?"
    "Je jette un oeil sur mon téléphone : 10 minutes de retard, et toujours pas de nouvelles de sa part."
    "Du peu que je connais Mei, elle est plutôt du genre à venir en avance."
    
    menu:
        "Que dois-je faire ?"
        "Envoyer un texto à Mei et attendre":
            "J’envoie un court texto pour lui dire que je suis déjà dans la bibliothèque et que je l’attends."
            "..."
            "Quelques minutes s’écoulent sans réponse à l’horizon. Inquiet, je décide de sortir pour lui téléphoner."
        "Sortir de la bibliothèque pour téléphoner à Mei":
            "Je laisse mes affaires sur ma chaise pour réserver la table puis je sors de la bibliothèque afin d’appeler Mei."
    
    scene school 
    
    "Arrivé dans la cour, je remarque que deux personnes entretiennent une conversation animée."
    
    show arata colere at tt_left
    show mei colere at center
    
    if natation == True :
        "C’est Mei et le garçon qui était avec elle au club de natation !"
    else :
        "C’est Mei et un autre garçon que je ne connais pas !"
    
    "Je crois que c’est un senpai."
    "Je m’approche pour tenter de parler à Mei, mais le ton monte et je n’ai pas le courage de m’imposer."
    
    "???" "C’est maintenant que je veux passer du temps avec toi. Après j’aurai plus envie."
    Mei "Je dois travailler sur un exposé avec le nouvel élève de ma classe !"
    "???" "Ok, je vois que t'as choisi."
    Mei "C’est toi qui me demande toujours de venir quand ça t’arrange ! Tu te plains quand j’ai prévu autre chose, mais moi aussi j’ai une vie en dehors de toi, Arata !"
    Arata "C’est ça. Un exposé de maths en binôme. T’as qu’à annuler avec l’autre et travailler une autre fois. Même si tu le faisais toute seule, tu aurais quand même une bonne note."
    Mei "Tu ne comprends rien ! Je-"
    
    "Mei semble enfin remarquer ma présence."
    
    Mei "A tout à l’heure, Arata."
    
    hide arata colere
    hide mei colere
    
    show mei normal at center
    
    "Furieux, l’autre garçon tourne les talons et me laisse seul avec Mei."
    
    Mei "Oh, %(mainchar)s. Désolée pour le retard et pour...ça"
    
    menu:
        "Que répondre ?"
        "Ce n'est rien":
            "Mei hoche la tête."
            Mei "C’était Arata, mon petit-ami. On est ensemble depuis quatre mois. Il est à l’université depuis la rentrée, et c’est un membre du club de natation de l’académie. Enfin, bref...Voilà."
        "C'était qui ce mec ?":
            Mei "C’était Arata, mon petit-ami. On est ensemble depuis quatre mois. Il est à l’université depuis la rentrée, et c’est un membre du club de natation de l’académie. Enfin, bref...Voilà."
        
    
    Mei "On va travailler ?"
    
    Moi "Oui, mes affaires sont déjà à l'intérieur."
    
    scene library
    
    show mei normal at center

    "Nous nous installons en silence autour de la table. Je suis un peu tendu et ne sais pas vraiment par où commencer."
    "De son côté, Mei sort les documents qu’elle a préparés."
    
    if devoir1 == False :
        Moi "Euh Mei...En fait, je n’ai rien préparé pour aujourd’hui. Je pensais qu’on ferait vraiment tout ensemble."
        "Mei m’adresse un sourire compréhensif."
        Mei "Ce n’est pas grave. Tu es nouveau et je sais qu’il te faut un peu de temps pour démarrer. Tiens, tu peux lire mes notes."
        "Tandis que je rattrape le travail déjà effectué par Mei, celle-ci se met en quête de livres qui pourraient nous être utiles pour l’exposé."
        "Nous travaillons ainsi, chacun de notre côté, pendant une bonne demi-heure."
    else :
        "Je sors des notes à mon tour et Mei m’adresse un large sourire."
        Mei "Ah toi aussi tu as préparé des choses ? Ca ira plus vite comme ça. On met en commun ?"
    
    "Notre dynamique de travail est soudainement affectée par le rire bruyant d’un autre élève, assis quelques tables plus loin."
    "Je l’observe montrer son téléphone à l’un de ses amis, qui éclate de rire à son tour."
    "Je tente de rester concentré sur ma lecture, mais c’est plus compliqué que ça en a l’air."
    "Je relève la tête et constate que Mei semble aussi agacée que moi."
    "Nous échangeons quelques regards, tentant de trouver la solution la plus appropriée pour mettre fin à cette situation."
    
    menu:
        "Que faire ?"
        "Ne rien dire et faire profil bas":
            "Se mettre dans le pétrin pour d’autres n’apporte jamais rien de bon. Je baisse les yeux et fais semblant de lire, même s’il devient de plus en plus difficile de se concentrer dans ces conditions."
            "Au bout de plusieurs minutes, la bibliothécaire intervient enfin pour remettre en place les élèves bruyants."
            "Les deux fauteurs de troubles haussent les épaules et quittent la pièce."
            "Mei soupire. Je lui adresse un sourire compatissant et replonge mon nez dans mes notes. Où en étais-je ?"
            
        "Dire à l'étudiant de se taire pour impressionner Mei":
            hide mei normal
            show randomStudent at center
            Moi "Tais-toi un peu. Il y a des gens qui aimeraient bien travailler."
            "Eleve" "Hein ? Occupe-toi de tes affaires."
            
            "Je me lève d’un coup de ma chaise. Cette situation est intolérable."
            Moi "On est dans une bibliothèque. Si tu veux jouer sur ton téléphone, tu n’as qu’à sortir."
            "Eleve" "Essaie donc de me faire sortir !"
            
            "Je suis prêt à en venir aux mains, mais la bibliothécaire débarque d’un coup. Tous les regards sont rivés sur nous."
            "Bibliothécaire" "Ca suffit ! Je ne veux pas savoir qui a commencé. Le prochain qui parle se verra privé d’accès à la bibliothèque pour 1 mois, c’est bien clair ?!"
            
            "Les deux fauteurs de troubles haussent et les épaules et quittent la pièce."
            hide randomStudent
            show mei embarrassed at center
            "Mei me tire alors la manche pour me demander de me rasseoir. Ses joues sont rouges pivoines, on dirait qu’elle est gênée. Je n’ai pourtant pas l’impression d’avoir mal agi."
            "Agacé, je décide de ne plus parler de cette histoire et me replonge dans mes notes. Où en étais-je ?"
            hide mei embarrassed
            show mei normal at center 
            
        "Faire une blague discrète sur l'impolitesse de l'étudiant":
            $ super +=1
            Moi "Tu ne trouves pas qu’il y en a un qui a un rire de hyène ? Sérieux, on se croirait dans Le Roi Lion !"
            "Mei étouffe un rire discret et approuve d’un signe de tête."
            Mei "C’est vrai que c’est vraiment pas commun comme rire."
            
            "Alertée par le bruit, la bibliothécaire intervient pour faire sortir les deux élèves bruyants."
            "Amusés, nous regardons les deux fauteurs de troubles hausser les épaules avant de quitter la pièce."
            "Nous rions encore un moment en imitant leur haussement d’épaules faussement désinvoltes, puis nous nous remettons au travail. Où en étions-nous ?"
            
        
    "Au bout d’une heure, nous avons chacun bien avancé dans nos recherches."
    "Il est l’heure de se récompenser pour notre travail."
    "Je farfouille un peu dans mon sac et sors un paquet de pocky à la banane, l’air triomphant."
    
    Moi "Sers-toi si tu veux."
    "Mei me sourit et me remercie. Nous commençons alors à piocher distraitement dans le paquet de friandises, jusqu’au moment où je sens quelque chose d’anormalement doux !"
    "C’est la main de Mei ! Sous la mienne !"
    "Mon coeur s’emballe aussitôt et je relève la tête, prêt à m’excuser, alors que je retire mes doigts de sa main."
    "Je pose mon regard paniqué sur son visage, à la recherche d’une réaction de sa part, mais elle semble indifférente et croque dans le biscuit en lisant, comme s’il ne s’était rien passé."
    "Le coeur encore agité, je baisse les yeux sur mon livre et tente de me raisonner."
    "C’est vrai, pour elle ce doit être banal. Après tout, elle a un petit-ami. Un contact de mains ne doit pas être quelque chose d’exceptionnel."
    "Mais pour moi ? Un lycéen sans aucune expérience romantique..."
    "Je me sens rougir. Je n’avais encore jamais été proche d’une fille, alors ça me fait bizarre."
    
    Mei "%(mainchar)s ?"
    
    "Je sursaute. A-t-elle lu dans mes pensées ?"
    "Je relève mes yeux vers elle."
    
    Mei "On a bien travaillé aujourd’hui. Je pense qu’on peut emprunter ces livres et travailler chez nous ?"
    Moi "Oui, bonne idée."
    
    "Mei me sourit, bien qu’elle semble aussi un peu énervée."
    "Je ne pose pas de questions et commence à remballer mes affaires. Pendant de temps, Mei est déjà partie voir la bibliothécaire pour emprunter les livres."
    "Quand elle revient vers moi, elle me tend le livre que j’avais commencé plus tôt et se dirige vers la sortie."
    "Lorsque nous arrivons dans la cour extérieure, je vois que le senpai de tout à l’heure l’attend un peu plus loin."
    
    scene school
    hide mei normal
    show arata normal at tt_left
    show mei embarrassed at center
    "Comprenant que j’ai remarqué sa présence, Mei rougit légèrement."
    Mei "En fait, Arata m’a envoyé un message quand on travaillait. Il m’a dit qu’il venait me chercher. Désolée, peut-être que tu aurais voulu qu’on reste un peu plus ensemble."
    
    menu:
        "Que répondre ?"
        "Non, je commençais à fatiguer":
            Moi "Ce n’est pas grave. Je commençais à fatiguer de toute façon."
            "Mei semble un peu plus rassurée."
            Mei "Bon eh bien. A lundi %(mainchar)s !"
        "J'espère qu'on restera plus longtemps la prochaine fois":
            "Mei semble un peu dépitée."
            Mei "Oui, je m'arrangerai."
            Arata "Mei, tu viens ?"
            Mei "Bon eh bien. A lundi %(mainchar)s !"
    
    hide mei embarrassed
    hide arata normal
    
    scene kitchen
    
    "Je prends un chemin opposé et rentre de suite chez moi."
    "Mes parents sont déjà là et m’interrogent sur ma journée. Nous passons le reste de l’après-midi à discuter du lycée, de Mei et de mon exposé. Bien sûr, j’évite soigneusement de mentionner Arata et l’incident du “toucher de main”."
    "A 19h, nous passons à table. Maman nous a préparé des okonomiyakis !"
    "Le repas se déroule dans la bonne humeur, même si mes parents ont l’air fatigués. D’ailleurs, ils partent tout de suite se coucher après la vaisselle, me laissant quartier libre pour la soirée."
    
    
    menu:
        "En montant dans ma chambre, je me demande que faire ce soir..."
        "Faire ses devoirs":
            "Je sors mes cahiers, le livre emprunté à la bibliothèque et travaille en écoutant un mix lofi hip-hop toute la soirée."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "Lire le blog de Mei":
            "Je décide d’en apprendre un peu plus sur Mei. Après tout, nous sommes dans la même classe et nous allons faire un exposé ensemble."
            "Je trouve rapidement un de ses profils en ligne et je finis sur son blog où je lis les derniers billets."
            "Tiens, il y en a un qui date d’aujourd’hui ! Elle y raconte sa journée et notamment l’anecdote des gens bruyants à la bibliothèque."
            "“Je déteste les gens mal élevés ou ceux qui prennent les autres de haut ! Au moins autant que les romans de science-fiction !” écrit-elle en faisant référence aux fauteurs de trouble."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "Lire un manga":
            "Après une journée comme celle-ci, j’ai besoin d’un peu de détente."
            "Je me dirige vers ma bibliothèque et sort cinq tomes de Naruto."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "Jouer aux jeux vidéo":
            "Après une journée comme celle-ci, j’ai besoin d’un peu de détente."
            "J’allume mon ordinateur et rejoins mes partenaires de jeu sur WoW."
            "Je ne vois pas le temps passer et lorsque je relève la tête, il est déjà l’heure d’aller me coucher."
        "Faire du sport":
            "Faire du sport me permettra d’évacuer le stress de la journée."
            "Je me mets en quête d’un programme sportif sur internet et commence en douceur avec des exercices de musculation simples."
            "Je finis la soirée par une bonne douche chaude avant d’aller me coucher."
    
    scene black
    show text "{color=#fff}Fin du Date 1{/color}"
    $ renpy.pause()
    
    jump monday1
    
    return
    
label monday1 :
    
    scene black
    show text "{color=#fff}09/04/2018{/color}"
    $ renpy.pause()
    
    scene room 
    "Lundi..."
    "Je n’ai pas vu le dimanche passer et nous voilà déjà lundi."
    "Heureusement, je n’ai qu’un seul cours aujourd’hui : 3h de travaux pratiques de chimie !"
    "Je fourre ma blouse dans mon sac et prends le chemin du lycée."
    scene science room
    "J’arrive 10 minutes avant le début du cours. D’autres élèves sont déjà installés devant leurs paillasses."
    
    show hisaka normal at center
    "Un rapide examen de la salle me permet de repérer Hisaka, seul à la 3ème rangée."
    
    Moi "Je peux me mettre avec toi ?"
    Hisaka "Si tu veux."
    
    "Je pose mes affaires à côté des siennes, enfile ma blouse et décide d’engager la conversation en attendant l’arrivée du prof."
    
    menu:
        "De quoi lui parler ?"
        "Le prof est sympa ?":
            Moi "Tu penses quoi du prof ? On a pas encore eu cours de chimie, j’espère que ça va bien se passer."
            Hisaka "Ca va. Il n’est pas trop sévère, mais il n’est pas laxiste non plus."
        "Tu aimes bien la chimie ?":
            Moi "Tu aimes bien la chimie ? C’est difficile comme matière je trouve…"
            Hisaka "Je me débrouille bien en sciences, la chimie ne fait pas exception."
        "Pourquoi tu es tout seul ?":
            Moi "Tu étais tout seul avant que j’arrive. Tu n’aimes pas les autres gens de la classe ?"
            hide hisaka normal
            show hisaka embarrassed at center
            "Hisaka se gratte l’arrière de la tête, visiblement gêné."
            Hisaka "Mes amis ne sont pas dans notre groupe de TP"
            hide hisaka embarrassed
            show hisaka normal at center
        "Il y a une fille qui te plaît dans la classe ?":
            Moi "Il y a de jolies filles dans la classe, tu crois pas ? Il y en a une qui te plaît ?"
            hide hisaka normal
            show hisaka embarrassed at center
            "Hisaka rougit, comme piqué à vif."
            Hisaka "Euh. Non, il n’y a personne."
            hide hisaka embarrassed
            show hisaka normal at center

    "Notre discussion n’a pas le temps d’aller plus loin car le prof arrive et nous distribue d’emblée le protocole de l’expérience d’aujourd’hui."
    "“Réactions acido-basiques” super, ça ne m’inspire pas beaucoup."
    "A tour de rôle, nous nous levons pour récupérer une paire de gants et des tubes à essai."
    "De retour derrière ma paillasse, je lis les instructions. "
    "Ok, il faut préparer deux tubes à essai dans lesquels il faut introduire différentes solutions. Il va falloir être minutieux"
    
    call screen consignes
    $ ok = _return
    if ok == True :
        jump quickgame2
    
    return

label quickgame2 :
    
    $ cont = 1 #continue variable
    $ arr_keys = ["a","z","e"] 
    $ i = 0
    while cont == 1 and i < 5:
        call qte_setup(0.5, 0.5, 0.01, renpy.random.choice(arr_keys), renpy.random.randint(1, 9) * 0.1, renpy.random.randint(1, 9) * 0.1) from _call_qte_setup_1
        # to repeat the qte events until it is missed
        $ i=i+1
        "{b}%(i)s{/b}"
    if cont == 0 :
        jump monday1part2fail #échec
    else :
        jump monday1part2success #réussite
    
label monday1part2fail :
    "Mon échec n'échappe pas aux yeux de l'enseignant qui nous assigne un devoir supplémentaire."
    "Super, je crois que je vais y passer la soirée..."
    jump monday1part2
    
    return
    

label monday1part2success :
    "Super ! On a réussi !"
    jump monday1part2
    
    return
    
label monday1part2 :
    
    "Après avoir schématisé nos expériences et noté nos observations, Hisaka et moi nous accordons une petite pause avant de réfléchir aux équations de réaction."
    Moi "Fiou. J’ai hâte de rentrer à la maison."
    Hisaka "Tu vis loin de l'académie ?"
    Moi "Non, à quelques minutes à pieds seulement. Et toi ?"
    Hisaka "J’habite à Hebi, 10 minutes en tram environ."
    Moi "C’est pas le quartier des riches ?"
    hide hisaka normal
    show hisaka embarrassed at center
    Hisaka "Si, enfin...Je vis en colocation avec Naoko donc c’est un peu différent"
    
    menu:
        "Naoko Tanaka ? La fille qui est dans notre classe ? Je ne savais pas qu’ils vivaient ensemble !"
        "Ne rien dire":
            "Je vois bien qu’il est gêné alors je n’insiste pas plus. "
        "Demander s'ils sont en couple":
            Moi "Vous sortez ensemble ?"
            Hisaka "Non non. On est juste amis."
            "Je n’insiste pas plus en voyant l’enseignant se rapprocher de notre rangée."
            
    hide hisaka embarrassed
    "Nous retournons à nos activités en faisant comme s’il ne m’avait rien dit."
    "Finalement, les 3 heures de travaux pratiques passent et le prof nous libère."
    "Je range rapidement ma blouse au fond de mon sac et me dirige vers ma salle de club. Les véritables activités commencent aujourd’hui !"
    
    if natation==True :
        jump monday1natation
    elif musique==True :
        jump monday1musique
    else :
        jump monday1cuisine
    
    return
    
label monday1natation :
    scene pool
    
    "Club de natation"
    
    return
    
label monday1musique :
    scene music club
    
    "Club de musique"
    
    return
    
label monday1cuisine :
    scene kitchen club
    
    "Club de cuisine"
    
    return