init python:

   # Step 1. Create the gallery object.
    g = Gallery()

    g.locked_button = "images/locked/locked.png" #this is the thumbnail image for ALL LOCKED gallery previews, found in the images folder
    
    # Step 2. Add buttons and images to the gallery.

   # A button that contains an image that automatically unlocks.
    g.button("soireeAkio") #this is the name/label associated with your button for a particular image, can be modified
    g.condition("persistent.unlock_soireeAkio") #this is the requirement/condition that must be met for this gallery image to unlock
    g.image("images/bg room.png", "images/akio normal.png") #this creates a gallery image that overlaps a foreground on top of a bg, you can also use a single flattened image here 
    
     #Step 3: generate the gallery and how it looks
screen gallery:

   # Ensure this replaces the main menu.
    tag menu

   # The background.
    add "images/school.jpg" #this is the bg image for the gallery; found in the images folder
    
    # A grid of buttons.
    grid 1 2:

        xfill True
        yfill True

        # Call make_button to show a particular button.
        add g.make_button("soireeAkio", "images/takuya normal.png", xalign=0.5, yalign=0.5)

        #return to main menu
        textbutton "Return" action Return() xalign 0.5 yalign 0.5